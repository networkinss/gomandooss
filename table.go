package main

import (
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
	//"github.com/jcelliott/lumber"
)

type Table struct {
	Name        string
	Key         string
	Columncount int
	Rowcount    int
	header      []string
	data        map[int][]string
	autoID      int
}

func (t *Table) New() *Table {
	t.header = []string{}
	t.data = make(map[int][]string)
	t.Columncount = 0
	t.Rowcount = 0
	return t
}

func (t *Table) setName(name string) {
	//	t = *Table{Name: name}
	t.Name = name
}
func (t *Table) getName() string {
	return t.Name
}

func (t *Table) setHeader(header []string) error {
	if len(header) == 0 {
		return errors.New("Data for setting header are empty.")
	}
	if t.Columncount > 0 {
		return errors.New("Header already set.")
	}
	//	if strings.ToLower(header[0]) != "rowid" {
	//		header = append([]string{"ROWID"}, header...)
	//	}
	t.header = header
	t.Columncount = len(header)
	return nil
}

//func (t *Table) addColumn(col string) error {
//	if len(col) == 0 {
//		return errors.New("New columnname must not be empty.")
//	}
//	t.header = append(t.header, col)
//	t.Columncount = len(t.header)
//	return nil
//}

/* Insert a line of data. */
func (t *Table) insert(line []string) error {
	if len(line) <= t.Columncount {
		var id int
		id = t.Rowcount
		//		line = append([]string{strconv.Itoa(id)}, line...)
		/* Actual insert. */
		t.data[id] = line
		t.Rowcount++
	} else {
		errors.New("Row too long. Not enough columns.")
	}
	return nil
}
func (t *Table) printAll() {
	var fieldMap map[string]string
	if len(t.header) == 0 {
		_ = fieldMap
	}
	t.print(make(map[string][]string), t.header)
}
func (t *Table) printFilter(mapFilter map[string][]string) {
	print(mapFilter, t.header)
}
func (t *Table) print(mapFilter map[string][]string, fields []string) {
	var val, field, lineValue string
	var line, filterValuelist []string
	var found, doFilter bool
	var i, n, count int
	var space []int
	space = make([]int, t.Columncount, t.Columncount)
	if t.Columncount != len(t.header) {
		fmt.Print("Error: table is not consistent.")
		return
	}
	for i, val = range t.header {
		space[i] = len(val)
	}
	/* Calculate length for each data. */
	for i, line = range t.data {
		if len(line) > t.Columncount {
			fmt.Print("Error: linelength (" + strconv.Itoa(len(line)) + ") larger than number of columns (" + strconv.Itoa(t.Columncount) + ").\n")
			return
		}
		for i, val := range line {
			if len(val) > space[i] {
				space[i] = len(val)
			}
		}
	}
	/* Print header. TODO: relation to data. TODO: only custom fields. */
	if len(fields) > t.Columncount {
		Message("Length of requested fields is invalid: requested="+strconv.Itoa(len(fields))+", columns="+strconv.Itoa(t.Columncount), 2)
		return
	}
	for i, val = range t.header {
		room := space[i] - len(val)
		Message("room: "+strconv.Itoa(space[i])+"\n", 1)
		val += strings.Repeat(" ", room+3)
		fmt.Printf("%v", val)
	}
	fmt.Print("\n")
	/* Horizontal line. */
	for i = 0; i < t.Columncount; i++ {
		count = count + space[i] + 3
	}
	count -= 3
	fmt.Print(strings.Repeat("-", count))
	fmt.Print("\n")
	/****************
	 *	Print data. *
	 ****************
	 */
	/* First make a sorting slice with the keys of the data map. */
	var keys []int
	for key, _ := range t.data {
		keys = append(keys, key)
	}
	sort.Ints(keys)
	//	fmt.Printf("Sorted keys: %v\n", keys)
	for key := range keys {
		line = t.data[key]
		//		fmt.Printf("line: %v\n", line)
		doFilter = false
		lineValue = ""
		for n, val = range line {
			field = t.header[n]

			filterValuelist, found = mapFilter[field]
			if found {
				for _, v := range filterValuelist {
					if v == val {
						doFilter = true
						break
					}
				}
			}
			if doFilter {
				continue
			}
			room := space[n] - len(val)
			Message("room: "+strconv.Itoa(space[n])+"\n", 1)
			lineValue += val + strings.Repeat(" ", room+3)
			//			fmt.Printf("%v", val)
		}
		if doFilter {
			continue
		}
		fmt.Printf("%v\n", lineValue)
	}

}

//func alignLine(val *[]string, space []int, max int) {

//	for i := range *val {
//		if len(val[i]) > max {
//			val[i] = val[i][:max-3] + "..."
//		} else {
//			for len(val[i]) < space[i] && len(val[i]) < max {
//				val[i] = val[i] + " "
//			}
//		}
//	}
//}
