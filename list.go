package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/jcelliott/lumber"
)

func ListXML(root string) (ld string) {
	var files []string

	files = getFolderxmls(root)
	if len(files) == 0 {
		ld = "No data xml found in " + root + ".\n"
		return
	}
	for _, str := range files {
		ld = ld + str + "\n"
	}

	return
}

/* Returns  a subset of the command list defined by the filter. */
func filteredList(cList []Command) []Command {
	var newList []Command
	for _, c := range cList {
		if checkFilter(c) {
			newList = append(newList, c)
		}
	}
	return newList
}
func filteredIndexList(indexList []int) []int {
	var newList []int
	for _, index := range indexList {
		if checkFilter(cAll[index]) {
			newList = append(newList, index)
		}
	}
	return newList
}
func ListData(cList []Command, doMacros bool) {
	fmt.Printf("%v\n", xmlRoot.Description)
	ListDataAll(cList, doMacros, false)
	return
}

/* List data */
func ListDataAll(cList []Command, doMacros bool, doShowMacroposition bool) {
	const FIELDCOUNT int = 6
	var space [FIELDCOUNT]int
	var maxcolumns, i int
	var val [FIELDCOUNT]string
	var ld string
	var macroStr string
	if len(cList) == 0 {
		fmt.Printf("No data defined yet in " + gDataPath + ".\n")
		return
	}
	if xmlConf.Maxfieldlength > 3 {
		maxcolumns = xmlConf.Maxfieldlength
	} else {
		maxcolumns = 60
	}
	/* Initial loop for getting lengths. */
	if doShowMacroposition {
		val[0] = "Macro position"
	}
	val[1] = "ID"
	val[2] = ""
	val[3] = "TYPE"
	val[4] = "VALUE"
	val[5] = "DESCRIPTION"

	for i = 0; i < FIELDCOUNT; i++ {
		space[i] = len(val[i])
	}
	for _, c := range cList {
		if len(strconv.Itoa(c.Macroposition)) > space[0] {
			space[0] = len(strconv.Itoa(c.Macroposition))
		}
		if len(strconv.Itoa(c.ID)) > space[1] {
			space[1] = len(strconv.Itoa(c.ID))
		}
		//				if len(c.Name) > space[2] {
		//					space[2] = len(c.Name)
		//				}
		if len(c.Handler) > space[3] {
			space[3] = len(c.Handler)
		}
		if len(c.Value) > space[4] {
			space[4] = len(c.Value)
		}
		if len(c.Description) > space[5] {
			space[5] = len(c.Description)
		}

	}
	/* calculate header line. */
	alignSpaceMax(&val, space, maxcolumns)
	for _, v := range val {
		ld += fmt.Sprintf("%v ", v)
	}
	ld += "\n"
	fiList := filteredList(cList)
	for _, c := range fiList {
		//		nr := nrsl[c.Handler]
		//if checkFilter(c) {
		if doShowMacroposition {
			val[0] = strconv.Itoa(c.Macroposition)
		}
		val[1] = strconv.Itoa(c.ID)
		val[2] = "" //c.Name
		val[3] = c.Handler
		val[4] = c.getShortValue(maxcolumns)
		val[5] = c.Description

		alignSpaceMax(&val, space, maxcolumns)

		for _, v := range val {
			ld += fmt.Sprintf("%v ", v) //	message(fmt.Sprintf(" %v\t\t%v\t\t%v\t\t%v", val, hrsl[nrsl[c.Handler]], c.Description, val))
		}
		ld += "\n"
	}
	/* Bring data to screen. */
	fmt.Print(ld)
	ld = ""
	if doMacros {
		fmt.Print("Macros\n")
		/* Now list also macros. */
		ListMacros()
		macroStr = " macro"
		if len(mAll) > 1 {
			macroStr = macroStr + "s"
		}
		fmt.Print(strconv.Itoa(len(fiList)) + " of " + strconv.Itoa(len(cList)) + " data and " + strconv.Itoa(len(mAll)) + macroStr + " listed from " + gDataPath + ".\n")
	} else {
		fmt.Print(strconv.Itoa(len(fiList)) + " of " + strconv.Itoa(len(cList)) + " data listed from " + gDataPath + ".\n")
	}
	return
}
func ListMacros() {
	const FIELDCOUNT int = 6
	var maxcolumns int
	var space [FIELDCOUNT]int
	var val [FIELDCOUNT]string
	var ld string
	if xmlConf.Maxfieldlength > 3 {
		maxcolumns = xmlConf.Maxfieldlength
	} else {
		maxcolumns = 60
	}
	/* Initial loop for getting lengths. */
	val[0] = "ID"
	val[1] = "MACRONAME"
	val[2] = "DESCRIPTION"
	val[3] = "NR OF STEPS"
	space[0] = len(val[0])
	space[1] = len(val[1])
	space[2] = len(val[2])
	space[3] = len(val[3])

	for _, m := range mAll {
		if len(strconv.Itoa(m.ID)) > space[0] {
			space[0] = len(strconv.Itoa(m.ID))
		}
		if len(m.Name) > space[1] {
			space[1] = len(m.Name)
		}
		if len(m.Description) > space[2] {
			space[2] = len(m.Description)
		}
		if len(strconv.Itoa(len(m.Steps))) > space[3] {
			space[3] = len(strconv.Itoa(len(m.Steps)))
		}
	}
	// Print header line.
	alignSpaceMax(&val, space, maxcolumns)
	for _, v := range val {
		ld += fmt.Sprintf(" %v ", v) //	message(fmt.Sprintf(" %v\t\t%v\t\t%v\t\t%v", val, hrsl[nrsl[c.Handler]], c.Description, val))
	}
	ld += "\n"

	// Print data.

	for _, m := range mAll {
		//		nr := nrsl[c.Handler]
		//if checkFilter(c) {
		val[0] = strconv.Itoa(m.ID)
		val[1] = m.Name
		val[2] = m.Description
		val[3] = strconv.Itoa(len(m.Steps))
		alignSpaceMax(&val, space, maxcolumns)

		for _, v := range val {
			ld += fmt.Sprintf(" %v ", v) //	message(fmt.Sprintf(" %v\t\t%v\t\t%v\t\t%v", val, hrsl[nrsl[c.Handler]], c.Description, val))
		}
		ld += "\n"
		//}

	}
	fmt.Print(ld)
	//	fmt.Print(strconv.Itoa(len(fiList)) + " of " + strconv.Itoa(len(cAll)) + " data listed from " + dataPath + ".\n")
	return
}

//TODO
func listMacroSteps(m Macro) (text []string) {
	var mapPos map[int]Macrostep
	var step Macrostep
	var pos []int
	mapPos = make(map[int]Macrostep)

	for _, step = range m.Steps {
		pos = append(pos, step.Position)
		mapPos[step.Position] = step
	}
	sort.Ints(pos)
	for _, i := range pos {
		step = mapPos[i]
		text = append(text, strconv.Itoa(i)+". ID: "+step.DataID+", name: "+step.Name+", description: "+step.Description+"\n")
	}
	return
}

/* Wiki table example.
{|
! style="text-align:left;"| Item
! style="text-align:left;"| NAME
! Amount
! Cost
|-
|Orange
|10
|7.00
|-
|Bread
|4
|3.00
|-
|Butter
|1
|5.00
|-
!Total
|
|15.00
|}
*/
/* List all data in wiki table format. */
func ListWiki(cAll []Command, pArea, pFilter string) (ld string) {

	var val [3]string
	var title string
	title = askforInput("Enter a title for the wiki table (or leave empty).")
	ld = "\n{| class=\"wikitable\" \n"
	if title != "" {
		ld += "|+ " + title + "\n"
	}

	if len(cAll) == 0 {
		ld = "No data defined yet.\n"
		return
	}
	/* Initial loop for getting lengths. */
	//	val[0] = ""
	//	val[2] = ""
	val[0] = "NAME" //style=\"text-align:left;\"|
	val[1] = "DESCRIPTION"
	val[2] = "VALUE"

	// Print header line.
	//alignSpace(&val, space)
	for _, v := range val {
		if v != "" {
			ld += fmt.Sprintf("!%v \n", v) //	message(fmt.Sprintf(" %v\t\t%v\t\t%v\t\t%v", val, hrsl[nrsl[c.Handler]], c.Description, val))
		}
	}
	ld += "\n"

	// Print data.
	fiList := filteredList(cAll)
	for _, c := range fiList {
		//		nr := nrsl[c.Handler]
		//if checkFilter(c) {

		//			val[0] = ""

		val[0] = c.Name
		//			val[2] = ""
		val[1] = c.Description
		val[2] = c.Value

		ld += "|-\n"
		for _, v := range val {

			ld += "|"
			ld += fmt.Sprintf("%v \n", v) //	message(fmt.Sprintf(" %v\t\t%v\t\t%v\t\t%v", val, hrsl[nrsl[c.Handler]], c.Description, val))
		}
		//			ld += "\n"
		//}

	}
	ld += "|}\n\n"
	return
}
func showValue(c Command) (msg string) {

	if c.Handler == HANDLERVALUE {
		msg = fmt.Sprintf("\nValue: %v\n", c.Value)
	} else if c.Handler == HANDLERWEB {
		msg = fmt.Sprintf("\nWeb URL: %v\n", c.Value)
	} else if c.Handler == HANDLERFILE {
		//		s := stringDecodeBase64(c.Value)
		//		propMap := makePropMap(c)
		//		msg = fmt.Sprintf("\nFile content:\n%v\nFile path:\n%v\n", s, propMap["path"])
		msg = fmt.Sprintf("\nFile path: %v\nFile content:\n%v\n", c.FilePath, c.getFileContent())
	} else {
		msg = fmt.Sprintf("\nData: %v\n", c.Value)
	}
	if len(c.AlternateValues) > 0 {
		msg += fmt.Sprintf("Alternate values: %v\n", strings.Join(c.AlternateValues, ", "))
	}

	return
}

/* Show alll atributes of one data. */
func getData(args string) Command {
	Message("args is:"+args, lumber.DEBUG)
	/* check if command exists */
	found, c, retMsg := searchComm(args)
	fmt.Print(retMsg)

	// if cannot find command prompt for creation of new data.
	if !found {
		var result int
		if askYesNo("Create new data") {
			newAny([]string{args})
			result = 0
		} else {
			result = 1
		}
		os.Exit(result)
	}
	return c
	//	fmt.Print(showValue(c))

}
func ShowMacro(args []string) {
	// check if command exists
	m, _ := searchMacro(args[0])

	// if cannot find command prompt creation of new
	if m.Name == "" {
		fmt.Print("Cannot find a macro with name " + args[0])
		os.Exit(1)
	}
	fmt.Print("ID: " + strconv.Itoa(m.ID) + "\nName: " + m.Name + "\nDescription: " + m.Description)
	fmt.Print("\nSteps\n")
	for _, str := range listMacroSteps(m) {
		fmt.Print(str)
	}
}

//TODO: make it possible to list dynamic fields. Add a field here for the doubled ID.
func listDataDoubles() {
	var doubleList []Command
	var doubleNrs []string
	var start int
	fmt.Printf("List of all items that have same value and description. Other fields like area, name, ID or tags are ignored.\n")

	start = 0
	for i, _ := range cAll {
		start++
		for n := start; n < len(cAll); n++ {
			if cAll[i].Value == cAll[n].Value && cAll[i].Description == cAll[n].Description {
				doubleList = append(doubleList, cAll[n])
				doubleNrs = append(doubleNrs, strconv.Itoa(cAll[n].ID))
			}
		}
	}
	ListData(doubleList, false)
	fmt.Printf("To remove them, copy next line of IDs and use parameter -remove <list of comma separated ids>.\n")
	fmt.Printf(strings.Join(doubleNrs, ",") + "\n")
}
func listAreas(learn bool, sensitive bool) {
	var areaMap map[string]int
	var area string
	var areaCount int
	var at Table
	areaMap = make(map[string]int)
	for i := 0; i < len(cAll); i++ {
		if sensitive {
			area = cAll[i].Area
		} else {
			area = strings.ToLower(cAll[i].Area)
		}

		areaCount = areaMap[area] + 1
		areaMap[area] = areaCount
	}
	at = *at.New()
	at.setHeader([]string{"Area", "Items"})
	fmt.Print("All areas and number of related items. \n")
	for k, v := range areaMap {
		at.insert([]string{k, strconv.Itoa(v)})
	}
	at.printAll()
}
