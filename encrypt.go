package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"reflect"

	"github.com/jcelliott/lumber"
)

// encrypt string to base64 crypto using AES
func encrypt(key []byte, text string) string {
	// key := []byte(keyText)
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func decrypt(key []byte, cryptoText string) string {
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		Message(fmt.Sprintf("Ciphertext too short: %v, does not fit blocksize %v\n", ciphertext, aes.BlockSize), lumber.ERROR)
		os.Exit(1)
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}

var iv = []byte{35, 46, 57, 24, 85, 35, 24, 74, 87, 35, 88, 98, 66, 32, 14, 05}

func encodeBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

/* Parameter: s string
   Returns true if decoding was ok.
   false if decoding caused an error.
*/
func decodeBase64(s string) ([]byte, bool) {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return []byte{}, false
	}
	return data, true
}
func stringDecodeBase64(stream string) string {
	b, ok := decodeBase64(stream)
	if ok {
		return string(b)
	} else {
		return "ERROR"
	}
}

func encryptText(text string) string {
	//	text = "1234567890" + text
	return encrypt([]byte(pPassword), text)
}
func encryptTextPwd(text string, pwd string) string {
	//	text = "1234567890" + text
	return encrypt([]byte(pwd), text)
}

func decryptText(text string) string {
	dec := decrypt([]byte(pPassword), text)
	//	dec = dec[10:]
	return dec
}
func decryptTextPwd(text string, pwd string) string {
	dec := decrypt([]byte(pwd), text)
	//	dec = dec[10:]
	return dec
}

func decryptData() {
	//	fmt.Printf("Len cAll: %v", len(cAll))
	encryptXML(&xmlRoot, false) //case data xml encryption
	//	fmt.Printf("Len cAll: %v", len(cAll))
	xmlRoot.Cmd = cAll
	xmlRoot.Description = decryptText(xmlRoot.Description)
}

func decryptConfig() {
	//case configuration xml decryption
	encryptXML(&xmlConf, false)
	xmlConf.Description = decryptText(xmlConf.Description)
}

func encryptData() {
	//case data xml encryption
	encryptXML(&xmlRoot, true)
	xmlRoot.Cmd = cAll
	xmlRoot.Description = encryptText(xmlRoot.Description)
}

func encryptConfig() {
	//case configuration xml decryption
	encryptXML(&xmlConf, true)
	xmlConf.Description = encryptText(xmlConf.Description)
}

// EncryptInternal Encryption or decryption of configuration and data xml. */
func EncryptInternal(what string, dataPath string) {
	/* En- De cryption data xml. */
	dataEncrypted := xmlRoot.Encrypted
	confEncrypted := xmlConf.Encrypted
	var taskData string
	var taskConf string
	taskData = "Data left unchanged."
	taskConf = "Configuration left unchanged."
	if dataEncrypted == "false" || dataEncrypted == "" {
		if what == "data" || what == "all" {
			//			encryptXML(&xmlRoot, true) //case data xml encryption
			//			xmlRoot.Description = encryptText(xmlRoot.Description)
			xmlRoot.Encrypted = "true"
			taskData = "Data encrypted."
		}
	} else if dataEncrypted == "true" {
		if what == "configuration" || what == "none" {
			/* mainly done already in main as standard for every task. */
			//			encryptXML(&xmlRoot, false) //case data xml decryption
			//			xmlRoot.Description = decryptText(xmlRoot.Description)
			xmlRoot.Encrypted = "false"
			taskData = "Data decrypted."
		}
	} else {
		taskData = "Unknown encryption status of " + dataPath + ": " + dataEncrypted
	}
	/* En- De cryption configuration.xml. */
	if confEncrypted == "false" {
		if what == "configuration" || what == "all" {
			//			encryptXML(&xmlConf, true) //case configuration xml encryption
			//			xmlConf.Description = encryptText(xmlConf.Description)
			xmlConf.Encrypted = "true"
			taskConf = "Configuration encrypted."
		}
	} else if confEncrypted == "true" {
		if what == "data" || what == "none" {
			/* mainly done already in main as standard for every task. */
			//			encryptXML(&xmlConf, false) //case configuration xml decryption
			//			xmlConf.Description = decryptText(xmlConf.Description)
			xmlConf.Encrypted = "false"
			taskConf = "Configuration decrypted."
		}
	} else {
		taskConf = "Unknown encryption status of " + gConfPath + ": " + confEncrypted
	}
	/* Save xml. */
	if dataEncrypted != xmlRoot.Encrypted {
		// TODO here datapath
		saveData(dataPath)
	}
	if confEncrypted != xmlConf.Encrypted {
		saveConfiguration()
	}
	Message(taskData, 0)
	Message(taskConf, 0)
}

/* Encrypt or decrypt xml fieldvalues. */
func encryptXML(xml interface{}, isEncrypt bool) {
	b, ok := decodeBase64(DEFAULTPASSWORD)
	s := string(b)
	if ok == false {
		Message("Decoding base64 failed.", lumber.ERROR)
		return
	}
	if pPassword == s {
		Message("You need to provide a password for encryption or decryption ( parameter -password=<password>.).", lumber.ERROR)

		os.Exit(1)
	}
	switch inst := xml.(type) {
	case *Root:

		/* Iterate all structs */
		for IDnr, c := range cAll {
			//fmt.Printf("ID: %v", IDnr)
			v := reflect.ValueOf(c)
			if isDebug {
				Message("Name: "+c.Name, lumber.DEBUG)
			}
			/* Iterate all fields of the struct. */
			for i := 0; i < v.NumField(); i++ {
				if v.Field(i).Type().String() == "string" && v.Type().Field(i).Name != "Encrypted" {
					value := v.Field(i).String()
					if isDebug {
						//						fmt.Printf("VALUE: %v \n", value)
					}
					if isEncrypt {
						reflect.ValueOf(&c).Elem().Field(i).SetString(encryptText(value))
					} else {
						reflect.ValueOf(&c).Elem().Field(i).SetString(decryptText(value))
					}

				}
				if isDebug {
					if v.Field(i).Type().String() == "string" {
						//						value := v.Field(i).Interface()
						//						fmt.Printf("VALUE: %v \n", value)
						//reflect.ValueOf(&c).Elem().Field(i).SetString("xxx")

					}
				}

			}
			cAll[IDnr] = c
		}
	case *Configuration:
		v := reflect.ValueOf(*inst)
		//typ := reflect.TypeOf(c)
		//typ := reflect.StructOf(c)
		//values := make([]interface{}, v.NumField())
		/* Iterate all fields of the struct. */
		for i := 0; i < v.NumField(); i++ {
			if v.Field(i).Type().String() == "string" && v.Type().Field(i).Name != "Encrypted" {
				value := v.Field(i).String()
				//				if len(value) < 24 && is_encrypt == false {
				//					i++
				//					continue
				//				}
				if isDebug {
					//val := reflect.Indirect(v)
					//name := v.Type().Field(i).Name
					//fmt.Printf("name: %v \n", name)
				}
				if isEncrypt {
					reflect.ValueOf(inst).Elem().Field(i).SetString(encryptText(value))
				} else {
					reflect.ValueOf(inst).Elem().Field(i).SetString(decryptText(value))

				}

			}
			if isDebug {
				if v.Field(i).Type().String() == "string" {
					//					value := v.Field(i).Interface()
					//					fmt.Printf("VALUE: %v \n", value)
					//reflect.ValueOf(&c).Elem().Field(i).SetString("xxx")

				}
			}

		}
	default:
		Message("Unknown xml type.", lumber.ERROR)
		os.Exit(1)
	}

}

//	else {
//		/* Decrypt all values. */
//		switch inst := xml.(type) {
//		case *Root:
//			for IDnr, c := range cAll {
//				//					fmt.Printf("ID: %v", IDnr)
//				v := reflect.ValueOf(c)
//				if isDebug {
//					p("Name: " + c.Name)
//				}
//				/* Iterate all fields of the struct. */
//				for i := 0; i < v.NumField(); i++ {
//					if v.Field(i).Type().String() == "string" {
//						value := v.Field(i).String()
//						if isDebug {
//							fmt.Printf("VALUE: %v \n", value)
//						}
//						reflect.ValueOf(&c).Elem().Field(i).SetString(decryptText(value))

//					}
//					if isDebug {
//						if v.Field(i).Type().String() == "string" {
//							value := v.Field(i).Interface()
//							fmt.Printf("VALUE: %v \n", value)
//							//reflect.ValueOf(&c).Elem().Field(i).SetString("xxx")

//						}
//					}

//				}
//				cAll[IDnr] = c
//			}
//		case *Configuration:
//			v := reflect.ValueOf(*inst)

//			for i := 0; i < v.NumField(); i++ {
//				if v.Field(i).Type().String() == "string" {
//					value := v.Field(i).String()
//					if len(value) < 24 {
//						i++
//						continue
//					}
//					if isDebug {
//						fmt.Printf("VALUE: %v \n", value)
//					}
//					reflect.ValueOf(inst).Elem().Field(i).SetString(decryptText(value))

//				}
//				if isDebug {
//					if v.Field(i).Type().String() == "string" {
//						value := v.Field(i).Interface()
//						fmt.Printf("VALUE: %v \n", value)
//						//reflect.ValueOf(&c).Elem().Field(i).SetString("xxx")

//					}
//				}

//			}
//		default:
//			p("unknow xml type")
//		}
//	}
//}
