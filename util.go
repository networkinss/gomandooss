package main

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"

	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/jcelliott/lumber"
	"golang.org/x/net/html"
)

var (
	xmlRoot    Root
	xmlConf    Configuration
	cAll       []Command
	mAll       []Macro
	dataIndex  map[string]int
	macroIndex map[string]int
	mrsl       []string
	hrsl       []string
	nrsl       map[string]int
	xmlID      int
	xmlMacroID int //TODO: not yet really used.
	//pathConfLinux string //"~/.gomando/"
	gPathRoot string

	// Flag vars to store flag value
	//var p = fmt.Println
	gDataPath, gConfPath      string
	gOps, gArea, gCurrentuser string

	pEncrypt                                  string
	pDataXML                                  string
	pDefaultXML                               string
	pPassword                                 string
	pYes                                      bool
	pTest                                     bool
	pCmdPassword, pArea, pFilter, pTags       string
	pPath, pServer, pPort, pPorthttps, pProxy string
	pText, pCommand, pUrl, pFile, pMacro      string
	pShow, pLimit                             string
	isDebug, isTUI, isNoshell                 bool
	scanner                                   = bufio.NewScanner(os.Stdin)
	iLoglevel                                 int
	doLogFile                                 bool
	doLogConsole                              bool
	log                                       lumber.Logger
)

const (
	HANDLERWEB   = "web"
	HANDLERMACRO = "macro"
	HANDLERVALUE = "text"
	HANDLERCMD   = "command"
	HANDLERFILE  = "file"
	//	HANDLERQUESTION  = "learning"
	HANDLERUNDEFINED = "undefined"
	//	LEARNFORMATFILE   = "FORMATFILE"
	LEARNFORMATINPUT   = "freeinput"
	LEARNFORMATOPTIONS = "options"

	/* Reserved names in xml. */
	DONCHECKIP = "DonCheckIP"

	PARAMINPUT  = "input"
	PARAMSTATIC = "static"
	PARAMREPEAT = "repeat"

	FRESHCONF = "Gomando configuration, delivered with new package installation."

	FILECONF = "configuration.xml"

	OPSUX  = "linux"
	OPSWIN = "windows"
	OPSMAC = "darwin"

	WIKISITE = "http://gomando.inss.ch"

	layout    string = "2006-01-02 15:04:05 (MST)"
	layoutNr2 string = "2006-01-02-15-04-05"

	savenow           string = "s"
	UNDEF             string = "undefined"
	DEFAULTPASSWORD          = "QF9FakJHRFJiOTRxP0tVcm5heD1oTnE0" //QF9FakJHRFJiOTRxP0tVcm5heD1oTnE0     @_EjBGDRb94q?KUrnax=hNq4
	VERSIONLIMITATION int    = 100
)

// TimeSorter sorts commands by time.
type TimeSorter []Command

func (a TimeSorter) Len() int           { return len(a) }
func (a TimeSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a TimeSorter) Less(i, j int) bool { return a[i].LastUsed.String() < a[j].LastUsed.String() }

// dataItemDesc reads a file at a given path and returns its description
func readRootXMLData(f string) (q Root, err error) {
	b, err := ioutil.ReadFile(f)
	if err != nil {
		return
	}

	// err causes bug trigger refer #12
	err = xml.Unmarshal([]byte(b), &q)
	return
}
func trim(param string, args []string) string {
	return strings.Trim(param+" "+strings.Join(args, " "), " ")
}

// dataList takes a directory
// it walks all the files in the dir and returns a list of xml config files with description
func dataList(dir string) (dataList string, dataNameList []string, err error) {
	i := 1
	err = filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if !f.IsDir() {
			rFound, err := regexp.MatchString(".xml", f.Name())
			if err == nil && rFound && f.Name() != FILECONF && i <= 5 {
				data, err := readRootXMLData(path)
				if err == nil {
					//return err
					var dataDesc string
					if data.Encrypted != "encrypt" {
						dataDesc = data.Description
					}
					dataList += fmt.Sprintf("F%v %v - %v\n", strconv.Itoa(i), f.Name(), dataDesc)
					dataNameList = append(dataNameList, f.Name())
					//dataList += fmt.Sprintln(f.Name(), "-", data.Encrypted, dataDesc)
					i++
				}
			} else {
				return err
			}
		} else {
			//return filepath.SkipDir
		}
		return nil
	})
	return
}

// lastCommandList generates a list of commands for the welcome ui content
func lastCommandList(path string) (lcl string, lclName []string, err error) {
	// find systemtype
	// Set path according to os type
	// path := dir + "linux.xml"

	data, err := readRootXMLData(path)
	if err != nil {
		return
	}

	if len(data.Cmd) > 0 {
		for i := 0; i < 5 && i < len(data.Cmd); i++ {
			//lcl += fmt.Sprintln(strconv.Itoa(i), data.Cmd[i].Name, "-", data.Cmd[i].Description)
			lcl += fmt.Sprintf("F%v %v - %v\n", i+6, data.Cmd[i].Name, data.Cmd[i].Description)
			lclName = append(lclName, data.Cmd[i].Name)
		}
	}

	return
}

/* Looks for a filter defined in parameter area or filter, and
   returns false if it filtered by area, tag or filter.
   returns true if no filter matches.
*/
func checkFilter(c Command) bool {
	var isFiltered = false
	var isArea = false
	var isTagged = false
	var found = []bool{false, false, false, false, false}
	if pArea == UNDEF {
		isArea = true
	} else {
		isArea = CiSearch(c.Area, pArea)
	}
	if pTags == UNDEF {
		isTagged = true
	} else {
		arTags := strings.Split(pTags, ",")
		for _, tag := range arTags {
			isTagged = isTagged || CiSearch(c.Tags, strings.Trim(tag, " "))
		}
	}
	if pFilter == UNDEF {
		isFiltered = true
	} else {
		found[0] = CiSearch(c.Name, pFilter)
		found[1] = CiSearch(c.Description, pFilter)
		found[2] = CiSearch(c.Tags, pFilter)
		found[3] = CiSearch(c.Area, pFilter)
		found[4] = CiSearch(c.Value, pFilter)
		for i := 0; i < len(found); i++ {
			isFiltered = isFiltered || found[i]
		}
	}
	isFiltered = isFiltered && isArea && isTagged
	return isFiltered
}

// CiSearch Search in string case insensitive.
func CiSearch(s, substr string) bool {
	s, substr = strings.ToUpper(s), strings.ToUpper(substr)
	return strings.Contains(s, substr)
}

/* Search in string case insensitive. */
/*func ciSearch(s, substr string) bool {
	s, substr = strings.ToUpper(s), strings.ToUpper(substr)
	return strings.Contains(s, substr)
}*/

/* helpList takes in the order of help msg from arrSort
it retrives the string msg form mHelp
Calculates the spaces between the options and orders accordingly
*/
func helpList(arrSort []string, mHelp map[string]string) (hl string) {
	var spaces string
	var i int
	for i = 0; i < len(arrSort); i++ {
		key := arrSort[i]
		val := mHelp[key]
		max := 10 - len(key)
		spaces = ""
		for il := 0; il < max; il++ {
			spaces += " "
		}
		if val == "" {
			hl += fmt.Sprintf("%s %s \n", key, spaces)
		} else {
			hl += fmt.Sprintf("-%s: %s %s\n", key, spaces, val)
		}
	}
	return
}

//not used.
// testing func may use again later
// searchComm checks wether command exists returns apporiate msg
//func chanFuncForTest(search string, cAll []Command, sc chan<- string) {
//	found := false
//	var c Command
//	for _, c = range cAll {
//		if c.Name == search {
//			found = true
//			break
//		}
//	}

//	var text string
//	if found {
//		text = fmt.Sprintf("%v\n%v\n%v", c.Name, c.Handler, c.Description)
//	} else {
//		text = fmt.Sprintf("Data with name %v not found.", search)
//	}
//	sc <- text

//	time.Sleep(5 * time.Second)
//	sc <- "test after 2"

//	return
//}

// makePropMap takes a command and returns a map of its properties
//func makePropMap(c Command) (propMap map[string]string) {
//	propMap = make(map[string]string)
//	for _, prop := range c.Properties {
//		propMap[prop.Key] = prop.Value
//	}
//	return
//}
/* searchComm goes over all commands stored in main data xml
and returns a bool for wether it found a command of the same name
and returns attributes of command if found.
*/
func searchComm(n string) (found bool, c Command, msg string) {
	var isNr bool
	var nrID int
	nrID, err := strconv.Atoi(n)
	isNr = (err == nil && nrID < 1000)
	Message("isNr = "+strconv.FormatBool(isNr), lumber.DEBUG)
	Message("nrID = "+strconv.Itoa(nrID), lumber.DEBUG)
	for _, c = range cAll {
		if c.Name == n || isNr && c.ID == nrID {
			found = true
			break
		}
	}
	if found {
		msg = fmt.Sprintf("ID: %v\nName: %v\nType: %v\nArea: %v\nTags: %v\nDescription: %v", c.ID, c.Name, c.Handler, c.Area, c.Tags, c.Description)

	} else {
		msg = fmt.Sprintf("Data with name %v not found.\n", n)
	}
	Message(msg, lumber.DEBUG)
	return
}

/* Removes one data by index from cAll.
   Returns true if successful.
*/
func removeByIndex(nr int) bool {
	if nr > len(cAll) || nr < 0 {
		Message(strconv.Itoa(nr)+" is outside the data index.", lumber.ERROR)
		return false
	}
	cAll = append(cAll[:nr], cAll[nr+1:]...)
	createIndex()
	return true
}
func removeMacroByIndex(nr int) bool {
	if nr > len(mAll) || nr < 0 {
		Message(strconv.Itoa(nr)+" is outside the macro index.", lumber.ERROR)
		return false
	}
	mAll = append(mAll[:nr], mAll[nr+1:]...)
	createIndex()
	return true
}

/* Ugly method of reducing return arguments from two to one. */
func reduceArg(s1 string, s2 string) string {

	return s1
}

/* Ugly method of reducing return arguments from two to one. */
//func reduceArgInt(i1 int, i2 interface{}) string {

//	return i1
//}

// setLastUsedComm takes a cammand and the master cAll slice
// it sets the c into cAll and saves the data using saveData
// it returns cAll so as to update it in the program
func setLastUsedComm(c Command, cAll []Command) []Command {
	// Set LastUsed time
	c.LastUsed = time.Now()
	// replace c into cAll array
	for i, v := range cAll {
		if c.Name == v.Name {
			cAll[i] = c
			break
		}
	}
	// Triger data saving
	saveData(gDataPath)
	// returns cAll so as to update cached copy of slice
	return cAll
}

//func alignSpace(val *[6]string, space [6]int) {
//	alignSpaceMax(val, space, 60)
//}
func alignSpaceMax(val *[6]string, space [6]int, max int) {
	//const max int = 40
	for i := range val {
		if len(val[i]) > max {
			val[i] = val[i][:max-3] + "..."
		} else {
			for len(val[i]) < space[i] && len(val[i]) < max {
				val[i] = val[i] + " "
			}
		}
	}
}

// closePipe closes a provided pipe when chan done is provided with a value
func closePipe(done chan int, pw *os.File) {
	<-done
	defer close(done)
	defer pw.Close()
}

func welMsgNew(currentuser string, version string) (logo string, welcomeMsg string) {
	_ = `
 /      \  /      \ /       \ 
/$$$$$$  |/$$$$$$  |$$$$$$$  |
$$ | _$$/ $$ |__$$ |$$ |  $$ |
$$ |/    |$$    $$ |$$ |  $$ |
$$ |$$$$ |$$$$$$$$ |$$ |  $$ |
$$ \__$$ |$$ |  $$ |$$ |__$$ |
$$    $$/ $$ |  $$ |$$    $$/ 
 $$$$$$/  $$/   $$/ $$$$$$$/  
                               
	`
	_ = `
MM'"""""'MM MMP"""""""MM M""""""'YMM 
M' .mmm. 'M M' .mmmm  MM M  mmmm. 'M 
M  MMMMMMMM M         'M M  MMMMM  M 
M  MMM   'M M  MMMMM  MM M  MMMMM  M 
M. 'MMM' .M M  MMMMM  MM M  MMMM' .M 
MM.     .MM M  MMMMM  MM M       .MM 
MMMMMMMMMMM MMMMMMMMMMMM MMMMMMMMMMM 
	`
	_ = `
 .----------------.  .----------------.  .----------------. 
| .--------------. || .--------------. || .--------------. |
| |    ______    | || |      __      | || |  ________    | |
| |  .' ___  |   | || |     /  \     | || | |_   ___ ` + "`" + `.  | |
| | / .'   \_|   | || |    / /\ \    | || |   | |   ` + "`" + `. \ | |
| | | |    ____  | || |   / ____ \   | || |   | |    | | | |
| | \ ` + "`" + `.___]  _| | || | _/ /    \ \_ | || |  _| |___.' / | |
| |  ` + "`" + `._____.'   | || ||____|  |____|| || | |________.'  | |
| |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------
`
	_ = `
 .d8888b.         d8888 8888888b.  
d88P  Y88b       d88888 888  "Y88b 
888    888      d88P888 888    888 
888            d88P 888 888    888 
888  88888    d88P  888 888    888 
888    888   d88P   888 888    888 
Y88b  d88P  d8888888888 888  .d88P 
 "Y8888P88 d88P     888 8888888P"   
`
	logo = `
   >===>          >>       >====>     
 >>    >=>       >>=>      >=>   >=>  
>=>             >> >=>     >=>    >=> 
>=>            >=>  >=>    >=>    >=> 
>=>   >===>   >=====>>=>   >=>    >=> 
 >=>    >>   >=>      >=>  >=>   >=>  
  >====>    >=>        >=> >====>
`
	welcomeMsg = "\nCopyright 2018 by International Network Support & Service - Glas (http://inss.ch).\n"
	welcomeMsg += "Homepage http://gomando.inss.ch\n"
	welcomeMsg += fmt.Sprintf("\nHello %v, I am Gad.\nI am a program to help with commands to be executed.\nUse -new to create a new data entry that you want to store.\nUse -help to get a list of all gad parameter.\nVersion: %v\n", currentuser, version)

	t := time.Now()
	if t.Month() == time.April && t.Day() == 1 {
		welcomeMsg += "\nAnd today is my birthday!"
	}

	return
}

//Sieb des Eratosthenes
func prim(args []string) {
	/* Argument parsing */
	var err error
	var iN, i uint64
	var zahl, zaehler uint64
	var count uint
	var doShow, doLast, doSieb, primzahl bool
	var limit string
	var primList []bool
	var t0, t1 time.Time
	doSieb = true
	for n := 0; n < len(args); n++ {
		if args[n] == "show" {
			doShow = true
		} else if args[n] == "last" {
			doLast = true
		} else if args[n] == "sieb" {
			doSieb = true
		} else {
			limit = args[n]
		}
	}

	if len(limit) > 1 {
		iN, err = strconv.ParseUint(limit, 10, 64)
		if err != nil {
			fmt.Printf("Not a number: %v", limit)
			os.Exit(1)
		}

	} else {
		iN = 100000
	}

	primList = make([]bool, iN) //var gestrichen: array [2..N] of boolean

	// Initialisierung des Primzahlfeldes
	for i = 2; i < iN; i++ {
		primList[i] = false
	}

	/* Start Sieb des Eratosthenes. */
	if doSieb {
		t0 = time.Now()
		for zahl = 2; zahl <= iN; zahl++ {
			primzahl = true
			for zaehler = 2; zaehler <= zahl/2; zaehler++ {
				if math.Mod(float64(zahl), float64(zaehler)) == 0 {
					primzahl = false
					break
				}
			}
			if primzahl == true {
				//println(zahl, " ist eine Primzahl")
				primList[zahl] = true
			}
		}
		t1 = time.Now()
	}
	/* End time. */
	for i = 1; i < iN; i++ {
		if primList[i] {
			if doShow {
				fmt.Printf("%v ", i)
			}
			count++
		}
	}
	if doShow {
		fmt.Printf("\n")
	}
	fmt.Printf("Found %v prime numbers till "+fmt.Sprint(iN)+".\n", count)
	if doShow {
		fmt.Printf("The calculation time does not include the time to print the prime numbers.\n")
	}
	fmt.Printf("The calculation took %v.\n", (t1.Sub(t0)))
	if doLast {
		for zahl = iN - 1; zahl >= 2; zahl-- {
			if primList[zahl] {
				fmt.Printf("Last and highest prime number is %v\n", zahl)
				break
			}
		}
	}

}

/* Check if path exists. True = exists. */
func CheckPath(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
		checkError(err)
	}
	return true
}

// CheckIP TODO
func CheckIP(pw *os.File) {
	var cip Command
	var nip, s string
	//	var p1, p2 int
	cip = searchCommand(DONCHECKIP)
	if cip.Value == "" {
		Message("No ip stored yet.", 0)
	} else {
		Message("Stored ip: "+cip.Value, 0)
	}
	if xmlConf.WebIPCheck == "" {
		Message("Youcan define a webpage which shows your external IP in the configuration.xml.", lumber.INFO)

		s = ReadWebpage("http://myexternalip.com/raw")
	} else {
		s = ReadWebpage(xmlConf.WebIPCheck)
	}
	//	p1 = strings.Index(s, "<body>")
	//	p2 = strings.Index(s, "</body>")
	//	s = s[p1+7 : p2]
	//	p1 = strings.Index(s, ":")
	nip = strings.TrimSpace(s)

	Message(fmt.Sprintf("Current IP: %s \n", nip), lumber.ERROR)

	if cip.Value == "" {
		Message("Storing IP "+nip, lumber.INFO)

		cip = getBareCommand(DONCHECKIP)
		//Command{Name: DONCHECKIP, ID: xmlID, Created: time.Now().Format(layout), CreatedBy: currentuser}
		cip.Handler = HANDLERVALUE
		cip.Description = "Don checkip value."
		cip.Area = gArea
		cip.Value = nip
		cip.Tags = "ip,check"
		cAll = append(cAll, cip)
		saveData(gDataPath)
	} else if nip == cip.Value {
		Message("No change.", lumber.INFO)

	} else {
		Message("IP changed to new iP "+nip, lumber.INFO)
		cAll[dataIndex[DONCHECKIP]].Value = nip
		saveData(gDataPath)
		mail(nip)
	}

}

// StringToSlice creates a slice from a string with comma as the delimiter
func StringToSlice(s string) []string {
	return strings.Split(s, ",")
}
func boolToString(yesno bool) string {
	if yesno {
		return "yes"
	} else {
		return "no"
	}
}
func GetFromTill(s string) (int, int) {
	strArray := strings.Split(s, "-")
	var from, till int
	var isnr bool
	if len(strArray) == 1 {
		till, isnr = checkIsNumber(strArray[0])
		if isnr {

			return 0, till
		} else {
			Message("Limit is not a number: "+s, lumber.WARN)
			return 0, 0
		}
	} else {
		till, isnr = checkIsNumber(strArray[1])
		if isnr == false {
			Message("Limit is not a number: "+s, lumber.WARN)
			return 0, 0
		}
		from, isnr = checkIsNumber(strArray[0])
		if isnr == false {
			Message("Limit from value is not a number: "+s, lumber.WARN)
			return 0, till
		}
	}
	return from, till
}
func StringRemoveTags(value string) string {
	var pos1, pos2, i, count int
	count = strings.Count(value, "<")
	if count <= 0 {
		//		Message("No < found in "+value, lumber.DEBUG)
		return value
	}

	for i = 0; i <= count; i++ {
		pos1 = strings.Index(value, "<")
		if pos1 > 0 {
			pos2 = strings.Index(value, ">") + 1
			if pos2 > pos1 {
				//				sval := value[pos1:pos2]
				//				Message("DEBUG sval: "+sval, 0)
				//				sval = askforInput(strconv.Itoa(i) + ": " + sval)
				//				value = value[:pos1] + sval + value[pos2:]
				value = value[:pos1] + value[pos2:]
				pos1 = pos2 + 1
			}
		}
	}
	return strings.Trim(value, " ")
}

/* Create index map. */
func createIndex() {
	/* Add data to index. */
	dataIndex = make(map[string]int)
	macroIndex = make(map[string]int)
	for i, c := range cAll {
		dataIndex[c.Name] = i
		if c.ID >= xmlID {
			xmlID = c.ID + 1
		}
	}
	/* Add macros to the same index map, so names must be unique for both data and macros. */
	for i, m := range mAll {
		macroIndex[m.Name] = i
		if m.ID >= xmlMacroID {
			xmlMacroID = m.ID + 1
		}
	}
}

/* Read piped input. */
func pipedInput() {
	reader := bufio.NewReader(os.Stdin)
	for {
		input, err := reader.ReadString('\n')
		if err != nil && err == io.EOF {
			break
		}
		endOL := len(input) - 1
		//		fmt.Printf("Länge1: %v", len(input))
		if input[endOL] == 10 {
			input = input[:endOL]
		}
		quickStore(input, false, HANDLERVALUE, "", true)
	}
	saveData(gDataPath)
}

func readLinks(file string) {
	if CheckPath(file) {
		contentByte, err := ioutil.ReadFile(file)
		checkError(err)
		ParseHTML(string(contentByte), pw)
		return
	}

	Message("Could not find file "+file+".", lumber.ERROR)
	os.Exit(1)

}

// ParseHTML Read html file like a bookmark.html.
func ParseHTML(content string, pw *os.File) {

	//	s := `<p>Links:</p><ul><li><a href="foo">Foo</a><li><a href="/bar/baz">BarBaz</a></ul>`
	doc, err := html.Parse(strings.NewReader(content))
	if err != nil {
		Message(err.Error(), lumber.ERROR)

		os.Exit(1)
	}
	var f func(*html.Node)
	f = func(n *html.Node) {
		link := ""
		textforlink := ""
		if n.Type == html.ElementNode && n.Data == "a" {
			nextsib := n.FirstChild
			if nextsib != nil {
				//				fmt.Println(nextsib.Data)
				textforlink = nextsib.Data
			}
			for _, a := range n.Attr {
				if a.Key == "href" {
					//					fmt.Println(a.Val)
					link = a.Val
					break
				}
			}
		}
		if link != "" {
			//			p("Link: " + link)
			//			p("Text: " + textforlink)
			quickStore(link, false, HANDLERWEB, textforlink, true)
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	saveData(gDataPath)
}

/* Reads a file and stores value as base64 encoded string.
*  Parameter
   path will be a property
   desc the Description field
   name of the struct
   tags of the struct
   action: EDIT=1, ADD=2
*/
func newFile(path string, desc string, name string, tags string, action int) Command {
	//TODO: take full command but as pointer as argument.
	var command Command
	//	var prop Properties
	//	var props []Properties
	//	const NOACTION int = 0
	const EDIT int = 1
	const ADD int = 2
	const ADDSAVE int = 3
	//	props = make([]Properties, 2)
	//	prop.Key = "path"
	//	prop.Value = path
	//	props[0] = prop

	fi, err := os.Stat(path)
	checkError(err)
	mode := fi.Mode()
	if fi.Size() > 1024000 {
		Message("Sorry, filestorage is meant only for small files with a maximum size of 1 MB.", lumber.ERROR)
		os.Exit(1)
	} else if fi.IsDir() {
		Message(path+" is a directory.", lumber.ERROR)
		os.Exit(1)
	} else {
		if mode&os.ModeSymlink == os.ModeSymlink {
			Message(path+" is a symbolic link.", lumber.ERROR)
			os.Exit(1)
		}
	}
	modep := fi.Mode().Perm()
	mi := uint32(modep)
	//	prop.Key = "permission"
	//	prop.Value = strconv.FormatUint(uint64(mi), 8)
	//	props[1] = prop

	contentByte, err := ioutil.ReadFile(path)
	checkError(err)
	str := encodeBase64(contentByte)
	if action == ADD || action == ADDSAVE {
		name = checkName(name)
		command = getBareCommand(name)
		command.File = str
		command.FilePermission = strconv.FormatUint(uint64(mi), 8)
		command.Value = path
		command.FilePath = path
		//		command.Properties = props
		command.Handler = HANDLERFILE
		command.LearnAnswerFormat = LEARNFORMATINPUT
		command.Description = desc
		command.Tags = tags
		if action == ADDSAVE {
			addCommand(command)
			saveData(gDataPath)
			printStoreInfo(command)
		}

	}
	if action == EDIT {
		command = searchCommand(name)
		command.File = str
		command.FilePermission = strconv.FormatUint(uint64(mi), 8)
		command.Value = path
		command.FilePath = path
		//		updateByName(command)
	}
	return command
}

/* Check if file path is valid. */
func checkFilepath(path string) string {
	var ok bool
	var count int
	for ok == false {
		if path == "" {
			path = askforInput("Please enter path to file to store.")
		}
		if CheckPath(path) == false {
			Message("Could not read file "+path+".", lumber.ERROR)

			count++
			path = ""
		} else {
			ok = true
		}
		if count > 5 {
			Message("Got no valid path to a file.", lumber.ERROR)

			os.Exit(1)
		}
	}
	return path
}

// ReadWebpage TODO
func ReadWebpage(url string) string {

	if isDebug {
		Message(fmt.Sprintf("HTML code of %s ...\n", url), 0)
	}

	resp, err := http.Get(url)
	// handle the error if there is one
	checkError(err)
	// do this now so it won't be forgotten
	defer resp.Body.Close()
	// reads html as a slice of bytes
	html, err := ioutil.ReadAll(resp.Body)
	checkError(err)
	// show the HTML code as a string %s
	//fmt.Printf("%s\n", html)
	return string(html)
}

/*	Puts main values of a command into a string.
	Shortens value to a maximum of 40 characters.
*/
func getShortDesc(c Command) string {
	var max int
	max = 40
	text := "Date to delete:\nID = " + strconv.Itoa(c.ID) + "\nname = " + c.Name + "\nDescription = " + c.Description
	if len(c.Value) > max {
		text = text + "\nBeginning of data = " + "..."
		text = text + c.Value[0:max]
	} else {
		text = text + "\nData = "
		text = text + c.Value
	}
	text = text + "\n"
	return text
}
func getMacroShortDesc(m Macro) string {
	var suf string
	text := "Macro to delete:\nID = " + strconv.Itoa(m.ID) + "\nname = " + m.Name + "\nDescription = " + m.Description
	if len(m.Steps) > 1 {
		suf = "s"
	} else {
		suf = ""
	}
	text = text + "\nMacro contains " + strconv.Itoa(len(m.Steps)) + " step" + suf + "."
	text = text + "\n"
	return text
}

/* Get Command as a one liner. Used by removemany. */
func getOneliner(c Command) (text string) {
	var max int
	max = 10
	text = "ID = " + strconv.Itoa(c.ID) + ", name = " + c.Name + ", description = "
	if len(c.Description) > max {
		text = text + c.Description[0:max] + "..."
	} else {
		text = text + c.Description
	}
	if len(c.Value) > max {
		text = text + ", data = "
		text = text + c.Value[0:max] + "..."
	} else {
		text = text + ", data = "
		text = text + c.Value
	}
	//	text = text + "/*\n*/"
	return text
}

/* Reading all xml files (no folders) from given path. */
func getFolderxmls(root string) (files []string) {
	files = make([]string, 0)
	f, err := os.Open(root)
	if err != nil {
		files = []string{"Could not read directory " + root + "."}
		return
	}
	fileInfo, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		files = []string{"Could not read files in directory " + root + "."}
		return
	}

	for _, file := range fileInfo {
		if file.IsDir() == false {
			if strings.HasSuffix(file.Name(), ".xml") && strings.HasPrefix(file.Name(), "configuration") == false {
				files = append(files, file.Name())
				//				ld += file.Name() + "\n"
			}
		}
	}
	return
}

/* Check if filename is .xml and if not add it. */
func checkXMLEnding(filename string) string {
	if len(filename) <= 4 {
		filename = filename + ".xml"
	} else if len(filename) > 4 && filename[len(filename)-4:] != ".xml" {
		filename = filename + ".xml"
	}
	return filename
}
func checkIsNumber(s string) (int, bool) {
	if nr, err := strconv.Atoi(s); err == nil {
		return nr, true
	} else {
		Message(s+" is not a number.", lumber.ERROR)
		return -1, false
	}
}
func correctFormat(filename string) (string, string) {
	var format string
	var suf string
	if xmlConf.StorageFormat != "" {
		format = strings.ToLower(xmlConf.StorageFormat)
		if format != "xml" && format != "json" {
			Message(format+" is not a valid storage format (xml or json), changing it to 'xml'.", lumber.WARN)
			format = "xml"
		}
	} else {
		format = "xml"
	}
	format = "." + format
	if len(filename) <= 4 {
		filename = filename + format
	} else if len(filename) > 4 {
		suf = filename[len(filename)-4:]
		if suf == "json" {
			suf = ".json"
		}
		if suf != format {
			if suf == ".xml" && format == ".json" {
				filename = filename[len(filename)-4:] + format
			} else if suf == ".json" && format == ".xml" {
				filename = filename[len(filename)-5:] + format
			} else {
				filename = filename + format
			}
		}
	}

	return format[1:], filename
}
func getFilename(path string) string {
	//	path := "/some/path/to/remove/file.name"
	return filepath.Base(path)

}
func printStoreInfo(c Command) {
	Message("Data is stored as "+c.Handler+". Call data with either ID '"+its(c.ID)+"' or name '"+c.Name+"'.", lumber.INFO)

}
func its(i int) string {
	return strconv.Itoa(i)
}

func watchForKill() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)
	<-c

	os.Exit(0)
}
