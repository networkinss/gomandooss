#!/bin/bash -i

export OUT=""
# collects info regarding push
echo Enter commit Message. Leave blank to skip git update or version increment
read gitMsg

# Provides a spinner showing task completion
### Not working...
check()
{
        if [ "$OUT" != "" ]
        then
          echo Build failed.
          echo $OUT
          exit 1
        else
         echo Build ok.
        fi
}
#spinner()
#{
#	local pid=$!
#	local delay=0.75
#	local spinstr='|/-\'
#	while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
#		local temp=${spinstr#?}
#		printf " [%c]  " "$spinstr"
#		local spinstr=$temp${spinstr%"$temp"}
#		sleep $delay
#		printf "\b\b\b\b\b\b"
#	done
#	printf "    \b\b\b\b"
#}

# building cross platform binaries
buildBinaries() {
	echo Starting builds for all...
	# Windows
	echo Starting build Windows...
        #go build & spinner
        #OUT=$(GOOS=windows GOARCH=386 go build 2>&1) 
        OUT=$(env GOOS=windows GOARCH=386 ./lb.sh build 2>&1)
	check
        mv gomando.exe ../../Gomando/gomando.exe

	# Mac build
	echo Starting build Mac...
        #OUT=$(GOOS=darwin GOARCH=amd64 go build 2>&1) 
        OUT=$(env GOOS=darwin GOARCH=amd64 ./lb.sh build 2>&1)
	check
	mv gomando ../../Gomando/gomando_mac

	#Linux
	echo Starting build Linux arm...
	#GOOS=linux GOARCH=arm go build & spinner
        #OUT=$(GOOS=linux GOARCH=arm go build 2>&1) 
        OUT=$(env GOOS=linux GOARCH=arm ./lb.sh build 2>&1)
	check
	mv gomando ../../Gomando/gomando_arm

	echo Starting build Linux 386...
        #GOOS=linux GOARCH=386 go build & spinner
        #OUT=$(GOOS=linux GOARCH=386 go build 2>&1) 
        OUT=$(env GOOS=linux GOARCH=386 ./lb.sh build 2>&1)
	check
        cp gomando ../paketierung/equivs
	mv gomando ../../Gomando/gomando_lnx
        echo binaries are now in folder workspace/Gomando.
 
	echo Build debian package
        pushd ~/workspace/go/Gosource/paketierung/equivs && equivs-build gomando.txt && cp *.deb ~ ; mv *.deb /home/amrit/workspace/go/Gomando ; popd
	echo copy to /usr/bin
	cp ../../Gomando/gomando_lnx /usr/bin/gomando

	echo build finished 
}

# git push
gitCommit () {
	# pushes latest to github
	echo Found gitMsg begining git script
	git add --all
	git commit -m "$gitMsg"
	git push
	echo Finished git script
}

# dep dependency tool
dep () {
	# optional dep management
	while getopts ":dep" opt; do
		# update dependencies of project
		echo Checking dependencies...
		dep ensure
		echo Updating dependencies...
		dep ensure -update
		echo Dependencies installed...
		dep status
	done
}

# main build flow
main () {
	# run version update script
	if [ "$gitMsg" != "" ];then
		#echo Update version nr...
		# updates version no
		#not needed anymore. ./iv.pl

		echo Start build in 5 sec...
		sleep 5

		dep
		buildBinaries
		gitCommit

	else
		echo Starting local build...
		./lb.sh
		buildBinaries
		echo local build finished execution
	fi

}

main 
echo Finished.

# $GOOS   $GOARCH
# android   arm
# darwin  386
# darwin  amd64
# darwin  arm
# darwin  arm64
# dragonfly   amd64
# freebsd   386
# freebsd   amd64
# freebsd   arm
# linux   386
# linux   amd64
# linux   arm
# linux   arm64
# linux   ppc64
# linux   ppc64le
# linux   mips
# linux   mipsle
# linux   mips64
# linux   mips64le
# netbsd  386
# netbsd  amd64
# netbsd  arm
# openbsd   386
# openbsd   amd64
# openbsd   arm
# plan9   386
# plan9   amd64
# solaris   amd64
# windows   386
# windows   amd64
