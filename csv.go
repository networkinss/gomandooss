package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/jcelliott/lumber"
)

func importCSV() int {
	var lan, countID int
	var header []string
	var headerPos []int
	var isHeader, found bool
	var temp string
	var cList []Command
	header = []string{"ID", "name", "value", "alternatevalues", "wrongvalues", "properties", "description", "tags", "parameter", "handler", "area", "blocker", "status", "macroposition", "learnstatus"}
	lan = len(header)
	headerPos = make([]int, lan, lan)
	for i, _ := range headerPos {
		headerPos[i] = -1
	}
	cList = make([]Command, 0)
	if CheckPath(pPath) == false {
		Message("The path "+pPath+" does not exist or is not readable.", lumber.ERROR)
		return 1
	}
	csvFile, _ := os.Open(pPath)
	reader1 := csv.NewReader(bufio.NewReader(csvFile))
	isHeader = true
	countID = 0
	for {
		line, error := reader1.Read()

		if error == io.EOF {

			break
		} else if error != nil {
			Message("Error reading csv file "+pPath, lumber.ERROR)
			return 1
		}
		if isHeader {
			for i, vl := range line {
				if vl == "" {
					continue
				}
				found = false
				for n, vh := range header {
					if strings.ToLower(vl) == strings.ToLower(vh) {
						headerPos[n] = i
						found = true
						break
					}
				}
				if found == false {
					Message("Not found in header: "+vl, lumber.WARN)
				}
			}
			isHeader = false
			//			fmt.Printf("%v\n", headerPos)

			continue
		}

		/* Error if description or value not found. */
		if headerPos[2] < 0 {
			Message("No field found for value. Value field is mandatory.", lumber.ERROR)
			return 1
		} else if headerPos[6] < 0 {
			Message("No field found for description. Description field is mandatory.", lumber.ERROR)
			return 1
		}

		c := Command{
			Value: line[headerPos[2]],
			//			AlternateValues: []string{line[headerPos[3]]},
			//			WrongValues:     []string{line[headerPos[4]]},
			//			Properties:      line[headerPos[5]],
			Description: line[headerPos[6]],
			//			Tags:        line[headerPos[7]],
			//			P:             line[headerPos[8]],

			//			Area:    line[headerPos[10]],
			//			Blocker:       line[headerPos[11]],
			//			Status:        line[headerPos[12]],
			//			Macroposition: atoi(line[headerPos[13]]),
			//			Learnstatus:   atoi(line[headerPos[14]])
		}

		/* handle ID not found*/
		if headerPos[0] < 0 {
			c.ID = countID
			countID++
			//			fmt.Printf("ID pos: %v\n", headerPos[0])
		} else {
			c.ID = atoi(line[headerPos[0]])
			//			fmt.Printf("ID linepos: %v\n", line[headerPos[0]])
		}

		/* Handle undefined name. */
		if headerPos[1] < 0 {
			c.Name = line[headerPos[2]] //if no name defined, take value instead.
		} else {
			c.Name = line[headerPos[1]]
		}
		if headerPos[9] < 0 {
			c.Handler = "value"
		}

		cList = append(cList, c)

	}
	temp = gDataPath
	gDataPath = pPath
	ListData(cList, false)
	//		people = append(people, Person{
	//			Firstname: line[0],
	//			Lastname:  line[1],
	//			Address: &Address{
	//				City:  line[2],
	//				State: line[3],
	//			},
	//		})

	peopleJson, _ := json.Marshal(cList)
	_ = peopleJson
	//	fmt.Println(string(peopleJson))
	fmt.Print("\n")
	if len(cAll) > 0 {
		Message("There are currently "+strconv.Itoa(len(cAll))+" data in "+temp+".", lumber.WARN)
	}

	b := askYesNo("Do you want to save now imported data into " + temp)
	if b {
		cAll = cList
		s := saveData(temp)
		if s {
			Message("Data stored into "+temp, lumber.INFO)
		} else {
			Message("Error saving data into "+temp, lumber.ERROR)
			return 1
		}
	}

	return 0
}
func atoi(value string) int {
	result, error := strconv.Atoi(value)
	if error != nil {
		return 0
	}
	return result
}
