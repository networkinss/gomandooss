#!/bin/bash -i
pushd /home/amrit/workspace/go/Gosource/gomando >/dev/null
go build -ldflags "-X main.version='0.9.`date +%Y%m%d.%H%M%S`'" 
if [ $? -ne 0 ]; then
echo build failed
exit 1
fi
if [ "$1" = "build" ]; then
exit 0
fi

sudo mv ./gomando /usr/bin

popd >/dev/null
