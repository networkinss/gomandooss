#!/usr/bin/python2
#
import psycopg2
import sys

# print 'Number of arguments:', len(sys.argv), 'arguments.'
# print 'Argument List:', str(sys.argv)
mypasswd = sys.argv[1]
#print(mypasswd)

try:
    db = psycopg2.connect("dbname='ginger' user='ginger' host='localhost' password='" +  mypasswd + "'")
except:
    print("Failure.")
    exit(1)
print("OK")
exit(0)