package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/jcelliott/lumber"
	"github.com/sadbox/mediawiki"
)

// uploadFile uses the mediawiki api to connect to www.pinpointwiki.com/gomando
// then open file, sanitize its name and upload
func uploadFile(xmlConf Configuration, pathToFile string) error {
	// check if file exists
	if CheckPath(pathToFile) == false {
		Message("Error: file "+pathToFile+" not found", lumber.ERROR)
		os.Exit(lumber.ERROR)
	}
	// check wether wiki details are set
	xmlConf, err := checkWikiDetails(xmlConf, gConfPath)
	if err != nil {
		Message(err.Error(), lumber.ERROR)
		os.Exit(2)
	}
	Message(fmt.Sprintf("Uploading %v", pathToFile), 0)

	Message("Creating mediawiki client...", 0)
	wikiClient, err := mediawiki.New("http://www.gomando.inss.ch/api.php", "Gomando")
	if err != nil {
		return fmt.Errorf("err creating new wikiClient: %v", err.Error())
	}
	// set the client details
	//wikiClient.Username = xmlConf.WikiUsername
	//wikiClient.Password = xmlConf.WikiPassword
	wikiClient.Domain = "http://www.gomando.inss.ch"
	//wikiClient.Domain = xmlConf.WikiDomain
	//Message(fmt.Sprintf("Logging in to wiki %v...", wikiClient), 0, pw)
	Message("Logging in to wiki...", 0)
	//os.Exit(0)
	//	err = wikiClient.Login()
	err = wikiClient.Login(xmlConf.WikiUsername, xmlConf.WikiPassword)
	if err != nil {
		return fmt.Errorf("err wiki login: %v", err.Error())
	}

	/*
		// READ A PAGE
		Message("reading page", 0, pw)
		page, err := wikiClient.Read("Main Page")
		if err != nil {
			Message(fmt.Sprintf("err wiki read: %v", err.Error()), lumber.ERROR, pw)
			return
		}
		fmt.Println(page.Body)
	*/

	// prepare file for upload
	Message("Preparing file for upload...", 0)
	f, err := os.Open(pathToFile)
	if err != nil {
		return fmt.Errorf("err wiki openfile: %v", err.Error())
	}
	defer f.Close()

	// upload file
	Message("Uploading file to wiki...", 0)
	//now := time.Now()
	//timeNow := fmt.Sprintf("d%v-%v-%v-t%v-%v-%v", now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute(), now.Second())
	uploadFileName := fmt.Sprintf("%v-%v-%v", xmlConf.WikiUsername, time.Now().UTC().Format(layoutNr2), filepath.Base(f.Name()))
	//uploadFileName := fmt.Sprintf("%v_%v_%v%v", filepath.Base(f.Name()), xmlConf.WikiUsername, timeNow, filepath.Ext(f.Name()))
	// TODO basename upload works only on the initial upload
	//uploadFileName := fmt.Sprintf("%v_%v", xmlConf.WikiUsername, filepath.Base(f.Name()))
	Message("Name of uploaded file shall be "+uploadFileName, lumber.INFO)
	err = wikiClient.Upload(uploadFileName, f)
	if err != nil {
		return fmt.Errorf("err wiki upload: %v", err.Error())

	}
	Message("Upload complete...", 0)
	return nil
}

// checkWikiDetails reads the xmlConf struct and checks we have the credentials
// required to upload to the pinpointwiki
func checkWikiDetails(xmlConf Configuration, confPath string) (Configuration, error) {
	var ok bool
	var loginurl string
	var wikidomain string
	Message("Your upload will be publicly available. If you don't want that, do not upload!", lumber.WARN)
	// sets the wiki domain path
	if xmlConf.WikiDomain == "" {
		wikidomain = "http://gomando.inss.ch/"
	} else {
		wikidomain = xmlConf.WikiDomain
	}
	if xmlConf.WikiLoginURL == "" {
		loginurl = "http://gomando.inss.ch/api.php"
	} else {
		loginurl = xmlConf.WikiLoginURL
	}

	wikiClient, err := mediawiki.New(loginurl, "gomando")
	if err != nil {
		return xmlConf, fmt.Errorf("err creating wikiClient: %v", err.Error())
	}

	for !ok {
		if xmlConf.WikiUsername == "" {
			Message("Please create your account on "+wikidomain+" to upload files", lumber.INFO)
		} else {
			Message("Checking wiki login with user "+xmlConf.WikiUsername, lumber.INFO)
			err = wikiClient.Login(xmlConf.WikiUsername, xmlConf.WikiPassword)
			if err != nil {
				Message("Incorrect wiki login credentials", lumber.ERROR)
			} else if err == nil {
				Message("Authenticated wiki login credentials", lumber.INFO)
				ok = true
			}
		}
		if !ok {
			// ask for username
			xmlConf.WikiUsername = askforInput("Please enter wiki username")
			// ask for password
			xmlConf.WikiPassword = askforInput("Please enter password")
			// save config
			save(xmlConf, confPath)
			Message("Credentials stored in "+confPath, lumber.INFO)
		}

	}
	return xmlConf, nil
}
