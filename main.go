package main

import (
	"flag"
	"fmt"

	//git clone https://github.com/golang/net.git
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/jcelliott/lumber"
)

var (
	mHelp   map[string]string
	arrSort []string
	// set them global to use from welcome screen
	fi     os.FileInfo
	binDir string
	pr     = os.Stdin
	pw     = os.Stdout
)

//go build -ldflags "-X main.version='0.8.`date +%Y%m%d.%H%M%S`'"
var version string = "0.0.0"

func main() {

	// Executable returs the path name for the executable that started the current process
	// TODO Note: advised to use path/filepath in docs

	/* TEST Flaggy

	 */
	// Declare variables and their defaults
	var stringFlag = "defaultValue"
	// Add a flag
	//flaggy.String(&stringFlag, "f", "flag", "A test string flag")
	// Parse the flag
	//	flaggy.Parse()
	// Use the flag
	_ = stringFlag
	/* END TEST Flaggy

	 */

	ex, _ := os.Executable()
	binDir = path.Dir(ex)

	// Add trailing /
	binDir = binDir + string(os.PathSeparator)

	// Stat returns fileinfo
	fi, _ = os.Stat(ex)
	gOps = runtime.GOOS
	gArea = gOps
	if gOps != "windows" {
		//pathConfLinux = os.Getenv("HOME") + string(os.PathSeparator) + ".gomando" + string(os.PathSeparator)
		gPathRoot = os.Getenv("HOME") + string(os.PathSeparator) + ".gomando" + string(os.PathSeparator)
	} else {
		gPathRoot = binDir
	}

	// Getenv returns the value of the environment varibale by key passed to it
	gCurrentuser = os.Getenv("USER")
	mrsl = []string{HANDLERVALUE, HANDLERCMD, HANDLERWEB, HANDLERFILE, HANDLERMACRO}
	hrsl = []string{"Text", "Command", "Web URL", "File", "Macro"}
	nrsl = make(map[string]int, len(hrsl))
	for i, c := range mrsl {
		nrsl[c] = i
		nrsl[hrsl[i]] = i
	}

	var isPiped bool
	/* Check if input is piped or not. */
	info, _ := os.Stdin.Stat()
	if gOps == "linux" {
		isPiped = info.Mode()&os.ModeNamedPipe != 0
	} else { //ToDo windows
		isPiped = false
	}

	// default behavior when no second argument is passed
	if len(os.Args) == 1 && !isPiped {

		// redirect to welcome screen
		// welcome(currentuser, version, d, xmlConf.HistoryEnabled, dataPath)

		// Cli welcome
		logo, welcomeMsg := welMsg(gCurrentuser, version)
		fmt.Println(logo, welcomeMsg)
		//view.Welcome(currentuser, version, d)
		os.Exit(0)
	}
	//TODO: define arguments to define what values are predefined like type=text, description=notext, area=area01

	// Steps to create a new command:
	// 1. Create an entry in switch case, and give arguments to function.
	// 2a. Create entry in map mHelp if it just a boolean switch.
	//      This adds it to flag list automatically.
	// 2b. If it is not boolean, it has to be added to flag.StringVar and it
	//     shall be executed before the swith statement with if pServer != UNDEF.
	//     Help text has to be added later in the non-boolean area.
	// 3. If it shall appear in the -help list, it must get an entry in arrSort.
	//    An entry in arrSort without mHelp data will show only the arrSort text (without -).

	/* Define help Message for each boolean type parameter. */
	mHelp = make(map[string]string)
	mHelp["list"] = "Lists all data stored."
	mHelp["listxml"] = "Lists all data xmls available."
	mHelp["listwiki"] = "Lists all data stored in wiki format."
	mHelp["listdoubles"] = "Lists all doubled data."
	mHelp["listareas"] = "Lists all doubled data."
	mHelp["help"] = "Shows help for parameter."
	mHelp["h"] = "Shows help for parameter."
	mHelp["home"] = "Opens http://gomando.inss.ch"
	//mHelp["show"] = "Shows the stored command for given ID. -show=<ID>."
	mHelp["showmacro"] = "Shows the stored macro, name in second argument."
	mHelp["add"] = "Creates new data and defines details in some questions."
	mHelp["addxml"] = "Creates a new data xml file."
	//	mHelp["text"] = "Stores all behind the argument as a value."
	//	mHelp["url"] = "Stores an URL that can be opened in a web browser."
	//	mHelp["command"] = "Stores all behind the argument as a command that can be executed."
	//	mHelp["macro"] = "Creates a workflow with a bunch of commands."
	mHelp["editmacro"] = "Edit an existing workflow."
	//	mHelp["wizard"] = "Not implemented. Starts a workflow to create a wizard for tasks with several commands."
	//	mHelp["file"] = "Read and store a file with path to file in second argument."
	mHelp["learn"] = "Learn data by seeing the description and check by entering the value. Not yet implemented."
	mHelp["learnlist"] = "Lists learned data with their status."
	mHelp["learnreset"] = "Resetting learning status."
	mHelp["set"] = "Set some values for this application."
	mHelp["block"] = "Not tested yet. Define conditions when a command should be blocked."
	mHelp["export"] = "Usage: -export -path=<path for xml export>. Exports data into a defined xml. Define filter with -filter=text or -area=text."
	mHelp["import"] = "Usage: -import -path=<path for xml to import>. Imports defined xml into current data xml. Define filter with -filter=text or -area=text."
	mHelp["importcsv"] = "Usage: -importcsv -path=<path for csv to import>. Imports csv file into current data xml."
	mHelp["edit"] = "Edit existing data with name in second argument."
	mHelp["editall"] = "Edit existing data with name in second argument."
	mHelp["remove"] = "Removes data from storage with name in second argument."
	//	mHelp["removemany"] = "Removes many data from storage. Filter data with -filter,-tag or -area filter."
	mHelp["removemacro"] = "Removes one macro from storage with name in second argument."
	mHelp["rename"] = "Renames data with old name in second and new name in third parameter."
	mHelp["readlinks"] = "Reads and stores links from a bookmark file."
	mHelp["checkip"] = "Checks external IP from current host and stores it. Notifies via mail in case of a change."
	mHelp["prim"] = "Calculating prim numbers until defined number from argument with default 100.000.000."
	mHelp["mail"] = "Sends mail with text from second argument with parameters defined in " + FILECONF + "."
	// mHelp["bashHistory"] = "Not Implemented yet"
	//	mHelp["last"] = "Runs last command executed via shell."
	mHelp["noshell"] = "Executing commands without shell. Default in Unix/Linux systems is /bin/bash."
	//	mHelp["tui"] = "Runs gomando in tui mode."
	mHelp["upload"] = "Uploads the default data xml and makes it publicly available at http://gomando.inss.ch/ (see also -dataxml)."
	mHelp["version"] = "Displays current build version number."

	// defines primary boolean flag values
	mArg := make(map[string]*bool)
	for key, val := range mHelp {
		mArg[key] = flag.Bool(key, false, val)
	}

	/* Check input parameter. */
	var arg1 string
	var macroName string
	/* Debug defaults to true if running inside debugger. */
	if fi.Name() == "main" {
		isDebug = true
	}

	/* declares flag values for parameter=value.
	   For each parameter add one flag, and one mHelp.
	   Flag message is printed in case of wrong arguments.
	   mHelp message is printed for -help parameter.
	*/
	sb, _ := decodeBase64(DEFAULTPASSWORD)
	flag.BoolVar(&isDebug, "debug", isDebug, "Debug mode usage: -debug")
	mArg["test"] = flag.Bool("test", false, "xxx")
	flag.BoolVar(&pYes, "yes", false, "Answer questions with yes automatically.")
	flag.StringVar(&pEncrypt, "encrypt", UNDEF, "Encryption of data. Usage: -encrypt=<none, configuration, data or all>.")
	flag.StringVar(&pDataXML, "dataxml", UNDEF, "Datastore xml to use.")
	flag.StringVar(&pLimit, "limit", UNDEF, "Limit number of data for export.")
	flag.StringVar(&pDefaultXML, "setdata", UNDEF, "Usage: setdata=<path to set default data xml>.")
	flag.StringVar(&pPassword, "password", string(sb), "Encryption password")
	flag.StringVar(&pCmdPassword, "cmdpassword", UNDEF, "Password for a single command.")
	flag.StringVar(&pArea, "area", UNDEF, "Usage: area=<filtervalue>.")
	flag.StringVar(&pText, "text", UNDEF, "Usage: -text=<text data>.")
	flag.StringVar(&pCommand, "command", UNDEF, "Usage: -command=<command to be executed>.")
	flag.StringVar(&pUrl, "url", UNDEF, "Usage: url=<webaddress>.")
	flag.StringVar(&pFile, "file", UNDEF, "Usage: file=<path to file>.")
	flag.StringVar(&pMacro, "macro", UNDEF, "Usage: macro=<name>")

	flag.StringVar(&pShow, "show", UNDEF, "Usage: -show=<ID>.")
	flag.StringVar(&pTags, "tag", UNDEF, "Usage: -tag=<tagtofilter>.")
	flag.StringVar(&pFilter, "filter", UNDEF, "Usage: filter=<text>.")
	flag.StringVar(&pPath, "path", UNDEF, "Usage: path=<export xml>.")
	flag.StringVar(&pServer, "server", UNDEF, "Usage: server=<type> with type = file or proxy.")
	flag.StringVar(&pPort, "port", UNDEF, "Usage: port=<number> with number = int.")
	flag.StringVar(&pPorthttps, "porthttps", UNDEF, "Usage: porthttps=<number> with number = int.")
	flag.StringVar(&pProxy, "proxy", UNDEF, "Usage: proxy=<proxy parameter like dump=.")
	/* Define help messages for non boolean parameter. Added later to avoid collision. */
	mHelp["encrypt"] = "This is not tested yet. Usage: -encrypt=<option>. Options are none, configuration, data or all. Will store xml values encrypted."
	mHelp["dataxml"] = "Usage: -dataxml=<path to xml>. Will load data from defined xml instead of default."
	mHelp["filter"] = "-filter=text filters for all data containing filter text in description, tags or area. Works only with -list or -export."
	mHelp["area"] = "Shows or exports only data with specified area (like area=linux)."
	mHelp["text"] = "Storing a text value: -text=<text data>."
	mHelp["command"] = "Storing a command: -command=<command with parameter>."
	mHelp["url"] = "Storing a web url: -url=<http://example.com>."
	mHelp["file"] = "Stores file content, path and permissoins: -file=<path to file>."
	/* later: mHelp["macro"] = "Define a macro: -macro=<value>." */
	mHelp["show"] = "Shows the stored command for given ID. -show=<ID>."
	mHelp["tag"] = "Shows or exports only data with specified tags. Seperate many tags with comma (like -tag=linux,user)."
	mHelp["server"] = "-server=<type> Starts either a simple file or proxy server. Type is \"file\" or \"proxy\"."
	mHelp["port"] = "Use with -server: -port=<number> with number=int."
	mHelp["setdata"] = " -setdata=<new xml path>. Change default data storage xml."
	mHelp["limit"] = "-limit=<number> Limits the number of output for export."
	//mHelp["path"] = "Usage: -export=<path for xml export>. Exports data into a defined xml. Define filter with -filter=text or -area=text."

	// Array to define order of printout of help.
	// Put all options also here to see them with -help.
	arrSort = []string{"Copyright 2018 by International Network Support & Service - Glas (http://inss.ch).", "General infos", "help", "home", "version", "list", "show", "New data", "add", "text", "command", "url", "file", "addxml", "Change data", "edit", "remove", "Tools", "export", "import", "importcsv", "upload", "readlinks", "checkip", "mail", "server", "Options", "setdata", "block", "encrypt", "filter", "limit", "area", "tag", "dataxml", "port"}

	flag.Usage = func() {
		arg1 = strings.TrimLeft(os.Args[1], "-")
		if mHelp[arg1] != "" {
			fmt.Printf("Usage of %s: %s\n", os.Args[1], mHelp[arg1])
		}
	}

	/* Parsinginput argument flags
	 * parse flags begin checks
	 */
	flag.Parse()

	if isDebug {
		Message("Debugging mode.", lumber.DEBUG)
	}
	/******************************************
	 * Handle data with parameter=value types.
	 * For some input parameter like -command
	 * all given parameters are joined
	 * to one parameter. Parameter like -show
	 *  have only one input parameter.
	 ******************************************/
	if pCommand != UNDEF {
		arg1 = "command"
		pCommand = trim(pCommand, flag.Args())
	} else if pText != UNDEF {
		arg1 = "text"
		pText = trim(pText, flag.Args())
	} else if pUrl != UNDEF {
		arg1 = "url"
		pUrl = trim(pUrl, flag.Args())
	} else if pFile != UNDEF {
		arg1 = "file"
		pFile = trim(pFile, flag.Args())
	} else if pMacro != UNDEF {
		arg1 = "macro"
		pMacro = trim(pMacro, flag.Args())
	} else if pServer != UNDEF {
		arg1 = "server"
	} else if pShow != UNDEF {
		arg1 = "show"
	}
	Message("pShow = "+pShow, lumber.DEBUG)

	/* Read configuration */
	if isDebug && fi.Name() == "main" {
		gConfPath = "/home/amrit/workspace/Gosource/gomando/configuration.xml"
	} else if gOps == OPSWIN {
		gConfPath = binDir + FILECONF
	} else {
		gConfPath = gPathRoot + FILECONF
	}
	// Checks if configuration.xml exists
	if _, err := os.Stat(gConfPath); os.IsNotExist(err) {
		Message("INFO: Missing configuration file "+gConfPath+", will be created now.", lumber.INFO)
		createConfiguration()
	} else {
		/* Read xml configuration. */
		readXMLConfiguration()
	}

	/* Define logging. */
	logMap := map[string]int{"DEBUG": lumber.DEBUG, "INFO": lumber.INFO, "WARN": lumber.WARN, "ERROR": lumber.ERROR}
	iLoglevel = logMap[strings.ToUpper(xmlConf.Loglevel)]
	if iLoglevel == 0 {
		iLoglevel = lumber.ERROR
	}
	doLogFile = strings.Contains(xmlConf.Logoutput, "file")
	doLogConsole = !strings.Contains(xmlConf.Logoutput, "noconsole") //TODO make it exact match
	if doLogFile {
		logPath := xmlConf.Logpath
		if logPath == "" {
			logPath = binDir + string(os.PathSeparator) + "gomando.log"
		}
		log, _ = lumber.NewFileLogger(logPath, iLoglevel, lumber.ROTATE, 5000, 3, 10)
	}
	if isDebug && log != nil {
		log.Level(lumber.DEBUG)
		Message("Logging Level Debug.", lumber.DEBUG)
	}
	/****************************************************
	*
	* Here handle arguments that do not need to load data.
	*
	*****************************************************/

	//	ch := make(chan os.Signal)
	//	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	//	go func() {
	//		Show()
	//		<-ch

	//		os.Exit(1)
	//	}()

	/*  setdata
	 *	Set default xml in configuration.xml and exit.
	 */
	if pDefaultXML != UNDEF {
		setXmldefault(pDefaultXML)
		os.Exit(0)
	}
	/* -newxml */
	if *mArg["addxml"] {
		newXmlstore(flag.Args(), gPathRoot)
		os.Exit(0)
	}

	/*************************
	*	Read data xml.
	*   	dataPath will be defined in setXMLData function.
	**************************/
	if pDataXML != UNDEF && pDataXML != "" {
		/* Check if -dataxml is defined by parameter -dataxml. */
		pDataXML = checkXMLEnding(pDataXML)
		if CheckPath(pDataXML) {
			gDataPath = pDataXML
		} else {
			gDataPath = gPathRoot + pDataXML
		}
	} else {
		gDataPath = UNDEF
	}
	gDataPath = loadData(gDataPath, fi, binDir)

	if xmlRoot.DefaultArea != "" {
		gArea = xmlRoot.DefaultArea
	}

	if *mArg["noshell"] {
		isNoshell = true
		delete(mArg, "noshell") //remove since it is not a parameter to be executed in the switch case statement.
	}
	if *mArg["help"] || *mArg["h"] {
		if len(flag.Args()) >= 1 {
			if strings.HasPrefix(os.Args[2], "-") {
				Message(flag.Arg(0)+": "+mHelp[strings.Trim(flag.Arg(0), "-")], 0)
			} else {
				Message("-"+flag.Arg(0)+": "+mHelp[flag.Arg(0)], 0)
			}
		} else {
			Message(helpList(arrSort, mHelp), 0)
		}
		os.Exit(0)
	}

	//if *mArg["tui"] {
	// calls the tui interface initializing it to the welcome screen, details of
	// the currentuser, version , whether history is enabled and path to the
	// data file to load are passed a nil ui is sent so as to enure proper
	// smooth transition to other xml data files.
	//	about(currentuser, version, d, xmlConf.HistoryEnabled, dataPath, nil)
	//	os.Exit(0)
	//}


	// Handle encryption state
	if pEncrypt != UNDEF {
		if pEncrypt == "none" || pEncrypt == "all" || pEncrypt == "configuration" || pEncrypt == "data" {
			arg1 = "encrypt"
			if pPassword == stringDecodeBase64(DEFAULTPASSWORD) {
				Message("Please provide password with parameter -password=<password>.", lumber.ERROR)

				os.Exit(1)
			} else {
				if len(pPassword) > 24 {
					fmt.Printf("Password must be shorter or equal to 24 characters (current: %x).", len(pPassword))
					fmt.Printf("Please define a shorter password.")
					os.Exit(1)
				}
				pPassword = pPassword + stringDecodeBase64(DEFAULTPASSWORD)[len(pPassword):]
				//						p("Password: " + pPassword)
				//						fmt.Printf("length: %x", len(pPassword))
			}
		} else {
			Message("Wrong parameter for -encrypt.", lumber.ERROR)
			Message(mHelp["encrypt"], lumber.ERROR)
			os.Exit(1)
		}
	}

	/* Define feature to do in arg1. */
	for key, val := range mArg {
		//fmt.Println("key", key)
		// checks wheteher any mArg has been set to true
		if *val {
			arg1 = key
			// fmt.Println("arg1", arg1)
			if arg1 == "help" {
				break
			}
		}
	}

	// Sets the macroName to the entered gomando command
	// eg gomando yaourt == macroName is yaourt
	if len(flag.Args()) > 0 {
		macroName = flag.Args()[0]
	}
	Message("Found argument: "+arg1, lumber.DEBUG)
	Message("Macro or data to execute: "+macroName, lumber.DEBUG)

	/* Check for piped input. */
	if isPiped {
		pipedInput()
		os.Exit(0)
	}

	/************************************
	*
	* Execute data with given name or ID.
	*
	************************************/
	if macroName != "" && arg1 == "" {
		os.Exit(runCommandFromName(macroName))
	}

	/***************************
	*
	* Check input parameter.
	*
	***************************/
	switch arg1 {
	case "test":

		//		StringRemoveTags(flag.Args()[0])
	case "learn":
		os.Exit(learn(flag.Args()))
	case "learnreset":
		learnreset()
	case "learnlist":
		learnlist()
	case "set":
		settings()
	case "add":
		// pipes to stdin and stdout steps for creation of new data returns a value
		// to done to signal closing of the pipe
		newAny(flag.Args())
		// blocks the execution till done signals closing of pipe
		//closePipe(done, pw)

	case "macro":
		newMacro(flag.Args())
	case "editmacro":
		editMacro(flag.Args())
	case "command":
		storeLine(HANDLERCMD, pCommand)
	case "text":
		//		if len(flag.Args()) < 1 {
		//			Message("Missing second argument for storing input.", lumber.ERROR)
		//			os.Exit(1)
		//		} else {
		quickStore(pText, true, HANDLERVALUE, pText, true)

		//storeLine(HANDLERVALUE, flag.Args())
		//			closePipe(done, pw)
		//		}
	case "file":
		quickFile(flag.Args())
	case "wizard":
		Message("Not yet implemented.", lumber.WARN)
	case "url":
		storeLine(HANDLERWEB, pUrl)
	case "show":
		//		if len(flag.Args()) < 1 {
		//			Message("Missing second argument for name of data.", lumber.ERROR)
		//			os.Exit(1)
		//		}
		c := getData(pShow)
		fmt.Print(showValue(c))
	case "showmacro":
		if len(flag.Args()) < 1 {
			Message("Missing second argument for name of macro.", lumber.ERROR)
			os.Exit(1)
		}
		ShowMacro(flag.Args())
	case "home":
		openURL(WIKISITE)
	case "list":
		ListData(cAll, true)
		os.Exit(0)
	case "listdoubles":
		listDataDoubles()
		os.Exit(0)
	case "listareas":
		listAreas(false, true)
		os.Exit(0)
	case "listxml":
		ld := ListXML(gPathRoot)
		fmt.Print(ld)
		os.Exit(0)

	case "listwiki":
		ld := ListWiki(cAll, pArea, pFilter)
		fmt.Print(ld)
		fmt.Print("Data listed from " + gDataPath + ".\n")
		os.Exit(0)
	case "export":
		if pPath == UNDEF {
			Message("Missing path parameter -path=<file> for export.", lumber.ERROR)
			os.Exit(1)
		}
		exportXML()
	case "import":
		if pPath == UNDEF {
			Message("Missing path parameter -path=<file> for import.", lumber.ERROR)
			os.Exit(1)
		}
		importXML()
	case "importcsv":
		if pPath == UNDEF {
			Message("Missing path parameter -path=<file> for import.", lumber.ERROR)
			os.Exit(1)
		}
		r := importCSV()
		os.Exit(r)

	case "edit":
		editData(flag.Args())

	case "upload":
		//Upload file into gomando.inss.ch wiki.
		err := uploadFile(xmlConf, gDataPath)
		if err != nil {
			Message(err.Error(), lumber.ERROR)
			os.Exit(2)
		}

	case "bashHistory":
		//bashHistory()
		fmt.Println("not implemented yet")

	case "prim":
		prim(flag.Args())
	case "readlinks":
		if len(flag.Args()) < 1 {
			Message("Missing path for file to read (like an exported bookmarks.html).", lumber.ERROR)

			os.Exit(1)
		} else {
			readLinks(flag.Arg(0))
		}
	case "block":
		if len(flag.Args()) < 1 {
			Message("Missing second argument for name of macro to be blocked.", lumber.ERROR)

			os.Exit(1)
		} else if len(flag.Args()) >= 1 {
			editBlocker(flag.Args(), gDataPath)
		}
	case "rename":
		if len(flag.Args()) < 2 {
			Message("Need to have two arguments: <oldname> <newname>.", lumber.ERROR)

			os.Exit(1)
		} else if len(flag.Args()) == 2 {
			rename(flag.Arg(0), flag.Arg(1), true)
		}
	case "remove":
		//		if len(flag.Args()) < 1 {
		//			Message("Missing name of macro for removal.", lumber.ERROR)
		//			os.Exit(1)
		//		} else {
		//			removeData(flag.Arg(0))
		//		}
		removeMany(flag.Args())
	case "removemacro":
		removeMacro(flag.Args())
		//	case "removemany":
		//		removeMany(flag.Args())
	case "server":
		/* Handles server command */
		runServer()
		os.Exit(0)
	case "checkip":
		CheckIP(pw)
	case "mail":
		if len(flag.Args()) < 1 {
			Message("Missing body text for mail.", lumber.ERROR)

			os.Exit(1)
		} else {
			mail(flag.Arg(0))
		}
	case "encrypt":
		EncryptInternal(pEncrypt, gDataPath)
	case "v":
		Message(fmt.Sprintf("Version: %s\n", version), 0)
	default:
		if strings.Trim(arg1, " ") != "" {
			Message("Parameter "+arg1+" is unknown.", lumber.ERROR)
			os.Exit(1)
		} else {
			os.Exit(0)
		}
	}
}

/* Make parameter values to one string. */
func buildParameter(parameter []Parameter) []string {
	var result []string
	for _, p := range parameter {
		//		result = append(result, " ")
		if p.Argtype == "static" {
			result = append(result, p.Value)
		} else if p.Argtype == "input" {
			fmt.Print(p.Description)
			scanner.Scan()
			result = append(result, scanner.Text())
		}
	}
	return result
}
func Info(text string) {
	Message(text, lumber.INFO)
}

/* Message Prints Messages on screen and also logging if configured. */
func Message(text string, logLevel int) {
	// retext := text

	if logLevel == 0 {
		fmt.Fprintln(pw, text)
		return
	}
	var f string
	if logLevel == lumber.ERROR || logLevel == lumber.DEBUG {
		pc, _, _, ok := runtime.Caller(1)
		details := runtime.FuncForPC(pc)
		if ok && details != nil {
			f = details.Name()
		}
	}

	text = f + " " + text
	if log != nil {
		if logLevel == lumber.DEBUG {
			log.Debug(text)
		} else if logLevel == lumber.INFO {
			log.Info(text)
		} else if logLevel == lumber.WARN {
			log.Warn(text)
		} else if logLevel == lumber.ERROR {
			log.Error(text)
		} else {
			log.Error(text)
			log.Error(fmt.Sprintf("Unknown error level: %v", logLevel))
		}
	}
	if isDebug && logLevel >= lumber.DEBUG || logLevel > lumber.DEBUG {
		fmt.Fprintln(pw, text)
	}

}

// welMsg  returns strings for initial greeting and logo message
func welMsg(currentuser string, version string) (logo string, welcomeMsg string) {
	logo = `
 ::::::::   ::::::::  ::::    ::::      :::     ::::    ::: :::::::::   ::::::::  
:+:    :+: :+:    :+: +:+:+: :+:+:+   :+: :+:   :+:+:   :+: :+:    :+: :+:    :+: 
+:+        +:+    +:+ +:+ +:+:+ +:+  +:+   +:+  :+:+:+  +:+ +:+    +:+ +:+    +:+ 
:#:        +#+    +:+ +#+  +:+  +#+ +#++:++#++: +#+ +:+ +#+ +#+    +:+ +#+    +:+ 
+#+   +#+# +#+    +#+ +#+       +#+ +#+     +#+ +#+  +#+#+# +#+    +#+ +#+    +#+ 
#+#    #+# #+#    #+# #+#       #+# #+#     #+# #+#   #+#+# #+#    #+# #+#    #+# 
 ########   ########  ###       ### ###     ### ###    #### #########   ########  

	`
	welcomeMsg = "\nCopyright 2018 by International Network Support & Service - Glas (http://inss.ch).\n"
	welcomeMsg += "Homepage http://gomando.inss.ch\n"
	welcomeMsg += fmt.Sprintf("\nHello %v, I am Gomando.\nI am a program to help with commands to be executed.\nUse -add to add new data that you want to store.\nUse -help to get a list of all gomando parameter.\nVersion: %v\n", currentuser, version)

	t := time.Now()
	if t.Month() == time.April && t.Day() == 1 {
		welcomeMsg += "\nAnd today is my birthday!"
	}

	return
}

/* Edit blocking conditions. */
func editBlocker(args []string, datapath string) {
	var c Command
	var n string
	var done []bool
	var ecount int
	done = []bool{false, false, false, false, false, false, false}
	if len(args) >= 1 {
		n = args[0]
	} else {
		Message("Missing value for macro name.", lumber.ERROR)

		os.Exit(1)
	}
	if nameExists(n) {
		c = searchCommand(n)
	} else {
		Message("Could not find an existing data with name "+n, lumber.ERROR)
		os.Exit(1)
	}
	b := c.Blocker
	ecount = 0
	Message("All time data are numbers. Separate multiple values with comma.", 0)
	Message("Empty values will block nothing.", 0)
	for ecount < 10 {
		if done[0] == false {
			b.Hours = askforInput("Please enter all hours comma separated for which the command shall be blocked (0-23).")
			done[0], _ = checkNumberList(b.Hours, 0, 23, -1)
			if done[0] == false {
				ecount++
				continue
			}
		}
		if done[1] == false {
			b.Weekday = askforInput("Please enter all days of the week to be blocked (0-6). Sunday is 0.")
			done[1], _ = checkNumberList(b.Weekday, 0, 6, -1)
			if done[1] == false {
				ecount++
				continue
			}
		}
		if done[2] == false {
			b.Monthday = askforInput("Please enter all days of the month to be blocked (1-31).")
			done[2], _ = checkNumberList(b.Monthday, 1, 31, -1)
			if done[2] == false {
				ecount++
				continue
			}
		}
		if done[3] == false {
			b.Hours = askforInput("Please enter all months to be blocked (1-12).")
			done[3], _ = checkNumberList(b.Hours, 1, 12, -1)
			if done[3] == false {
				ecount++
				continue
			}
		}
		if done[4] == false {
			b.Password = askforInput("Please enter a password. Password will be validi only for this command.")
			b.Password = encryptTextPwd(b.Password, DEFAULTPASSWORD)
			done[4] = true
		}
		if done[5] == false {
			b.WhitelistUser = askforInput("Define list of users for whitelist. All other users will be blocked.\nEmpty is allowance to everyone.")
			done[5] = true
		}
		break

	}
	if ecount > 10 {
		Message("Got no valid input for blocking conditions.", lumber.ERROR)

		os.Exit(2)
	}
	c.Blocker = b
	updateByName(c)
	saveData(datapath)

}

/* Edit existing data. */
func editData(args []string) bool {
	var justexit, keepit, nr, maxlength, option int
	var value string
	var listOps []string
	var doAsk bool
	if len(args) < 1 {
		Message("Need an existing data name. Look for a list of data with parameter -list.", lumber.ERROR)
		return false
	}
	var cmd Command
	maxlength = 60
	doAsk = true
	cmd = searchCommand(args[0])
	if cmd.Name == "" {
		Message("Name \""+args[0]+"\" not found in data store. Look for a list of data with parameter -list.", lumber.ERROR)
		return false
	}
	Message("\nYou can define new values or attribues now for '"+cmd.Value+"'.", 0)

	//	saveexit = len(listOps)
	option = 0
	for true {
		listOps = []string{"Datatype = " + cmd.Handler,
			"Data = " + cmd.getShortValue(maxlength),
			"Description = " + cmd.Description,
			"Tags = " + cmd.Tags,
			"Area = " + cmd.Area,
			"Show learning settings"}

		listOps = append(listOps, "Filepath = "+cmd.FilePath)
		listOps = append(listOps, "Save & continue adding data")
		listOps = append(listOps, "Save & exit")
		//listOps = append(listOps, "CTRL+c to cancel")
		if doAsk {
			option, _ = askWithOptions(listOps, false, "Choose what you want to change.", "\n ")
		} else {
			doAsk = true
		}

		nr = 0
		/* Starting with 0, therefore visible menu (nr -1) for the related case statement. */
		switch option {
		case 0:
			typeOps := hrsl
			typeOps = append(typeOps, "Keep it \""+hrsl[nrsl[cmd.Handler]]+"\"")
			keepit = len(typeOps) - 1
			typeOps = append(typeOps, "Cancel")
			justexit = len(typeOps) - 1

			nr, _ = askWithOptions(typeOps, false, "Choose the type of the data.\n", "   ")
			if nr == justexit {
				Message("Exit without saving changes.", 0)
				return false
			} else if nr == keepit {
				Message("No change.", 0)
			} else {
				cmd.Handler = mrsl[nr]
				Message("New datatype: "+cmd.Handler, 0)
			}

		case 1:

			value = askforInput("Enter new value (press return to keep).")
			if value != "" {
				cmd.Value = value
			} else {
				Message("Value is empty.", lumber.ERROR)
			}

		case 2:
			value = askforInput("Current description: '" + cmd.Description + "'\n. Enter new description (leave empty to keep).")
			if value != "" {
				cmd.Description = value
			}
		case 3:
			value = askforInput("Current tags: \"" + cmd.Tags + "\". Enter new tags (leave empty to keep).")
			if value != "" {
				cmd.Tags = value
			}
		case 4:
			value = askforInput("Current area: \"" + cmd.Area + "\". Enter new area (leave empty to keep).")
			//			nr, value = askWithOptions([]string{"Keep it \"" + cmd.Area + "\""}, true, "New area. ", "   ")
			if value != "" {
				cmd.Area = value
			}
		case 5:
			editLearning(cmd)
			//			value = askforInput("Add alternate value (leave empty to keep, enter 'd' to empty).")
			//			a := cmd.AlternateValues
			//			if value == "d" {
			//				a = []string{}
			//			} else {
			//				b := StringToSlice(value)
			//				a = append(a, b...)
			//			}
			//			cmd.AlternateValues = a
			//		case 6:
			//			value = askforInput("Answer format is " + cmd.LearnAnswerFormat + ". New input ('" + LEARNFORMATINPUT + "' or '" + LEARNFORMATOPTIONS + "':")
			//			if value == LEARNFORMATINPUT || value == LEARNFORMATOPTIONS {
			//				cmd.LearnAnswerFormat = value
			//			} else {
			//				fmt.Print("Wrong value, must be either 'single','list' or 'multi'.\n") //TODO: add file.
			//			}

			//		case 7:
			//			value = askforInput("Hidden for learning (" + boolToString(cmd.Learnhide) + "). Enter yes/no as value.")
			//			if strings.ToLower(value) == "yes" || strings.ToLower(value) == "y" {
			//				cmd.Learnhide = true
			//			} else {
			//				cmd.Learnhide = false
			//			}
			/* Before here add new options. */
		case 6:
			if cmd.Handler == HANDLERFILE {
				value = askforInput("Enter path to load new file.")
				if value != "" {
					cmd = newFile(value, cmd.Description, cmd.Name, cmd.Tags, 1)
				} else {
					Message("Filepath is empty.", lumber.ERROR)
				}
			} else {

				Message("Change datatype to file to load a file.", lumber.ERROR)
			}
		case 7: /* Save & add more */
			updateByName(cmd)
			if saveData(gDataPath) {
				Message("Data stored to filesystem.", lumber.INFO)

			} else {
				Message("Error while storing data.", lumber.ERROR)
				return false
			}
			newAny([]string{})
			return true
		case 8: /* Save&Exit */
			updateByName(cmd)
			if saveData(gDataPath) {
				Message("Data stored to filesystem.", lumber.INFO)
				return true
			}
			Message("Error while storing data.", lumber.ERROR)
			return false
		default:
			//			Message("Fehler", 0)
		}
		//		updateByName(cmd)

	}
	return true
}
func editLearning(cmd Command) {
	//	var justexit, keepit, nr, maxlength int
	var option int
	var value string
	var listOps []string
	var doAsk bool
	doAsk = true

	for {
		format := cmd.LearnAnswerFormat
		if format == "" {
			format = "undefined"
		}
		listOps = []string{"Alternate correct values: " + strings.Join(cmd.AlternateValues, ","),
			"Learning answer format: '" + format + "'",
			"Learning hidden: " + boolToString(cmd.Learnhide),
			"How many correct answers are mandatory: " + strconv.Itoa(cmd.LearnNumberValues),
			"Wrong answers: " + strings.Join(cmd.WrongValues, ",")}

		listOps = append(listOps, "Return")
		listOps = append(listOps, "Save&Exit")
		listOps = append(listOps, "CTRL+c to cancel")
		if doAsk {
			option, _ = askWithOptions(listOps, false, "Settings for learning mode for '"+cmd.Name+"'.", "\n")
		} else {
			doAsk = true
		}
		switch option {
		case 0:
			value = askforInput("Add alternate value (leave empty to keep, enter 'd' to empty).")
			a := cmd.AlternateValues
			if value == "d" {
				a = []string{}
			} else {
				b := StringToSlice(value)
				a = append(a, b...)
			}
			cmd.AlternateValues = a
		case 1:
			value = askforInput("Answer format is " + format + ". New input ('" + LEARNFORMATINPUT + "' or '" + LEARNFORMATOPTIONS + "'):")
			if value == LEARNFORMATINPUT || value == LEARNFORMATOPTIONS {
				cmd.LearnAnswerFormat = value
			} else if value == "" {

			} else {
				fmt.Print("Wrong value, must be either '" + LEARNFORMATINPUT + "' or '" + LEARNFORMATOPTIONS + "'.\n")
				doAsk = false
			}

		case 2:
			value = askforInput("Hidden for learning (" + boolToString(cmd.Learnhide) + "). Enter yes/no as value.")
			if strings.ToLower(value) == "yes" || strings.ToLower(value) == "y" {
				cmd.Learnhide = true
			} else {
				cmd.Learnhide = false
			}
		case 3:
			value = askforInput("How many right answer must be choosen (current: " + strconv.Itoa(cmd.LearnNumberValues) + ").")
			if nr, ok := checkIsNumber(value); ok {
				cmd.LearnNumberValues = nr
			} else {
				Message("Please enter a number.", lumber.INFO)
			}
		case 4:
			value = askforInput("Add wrong values for showing as options in learnmode (leave empty to keep, enter 'd' to empty).")
			a := cmd.WrongValues
			if value == "d" {
				a = []string{}
			} else {
				b := StringToSlice(value)
				a = append(a, b...)
			}
			cmd.WrongValues = a

		case 5: /**/
			return
		case 6: /* Save&Exit */
			updateByName(cmd)
			if saveData(gDataPath) {
				Message("Data stored in filesystem.", lumber.INFO)
				os.Exit(0)
			}
			Message("Error while storing data.", lumber.ERROR)
		default:
			//			Message("Fehler", 0)
		}
	}
}

//TODO finish.
func editMacro(args []string) {
	var name, answer string
	var index int
	var found bool
	var idList []string
	var mapFilter map[string][]string
	var m Macro
	var c, cFilter Command
	var steps []Macrostep

	const OPTMACRODESCRIPTION int = 0
	const OPTADDSTEP int = 1
	const OPTEDITSTEP int = 2
	const OPTHIDECOMMAND int = 3
	const OPTLISTCOMMANDS int = 4
	const OPTCANCEL int = 5
	const OPTSAVEEXIT int = 6

	header := []string{"Macro position", "ID", "NAME", "TYPE", "DESCRIPTION", "VALUE"}
	steps = make([]Macrostep, 0)
	mapFilter = make(map[string][]string)

	if len(args) == 0 {
		name = askforInput("Please enter name/id for the macro that you want to edit.")
	} else {
		name = args[0]
	}
	m, index = searchMacro(name)
	//	var justexit, keepit, nr int
	//	var value string
	var listOps []string

	if m.Name == "" {
		Message("Need an existing macro name or id. Look for a list of macros with parameter -list.", lumber.ERROR)
		return
	}

	Message("You can define now steps and descriptions for \""+m.Name+"\".", lumber.INFO)
	t := new(Table)
	t.New()
	t.setHeader(header)
	for _, c = range cAll {
		t.insert([]string{strconv.Itoa(c.Macroposition), strconv.Itoa(c.ID), c.Name, c.Handler, c.Description, c.Value})
	}
	for true {
		/* First list defined steps. */
		fmt.Print("\nCurrent macro steps:\n")
		for _, str := range listMacroSteps(m) {
			fmt.Print(str)
		}

		listOps = []string{"Macro description = " + m.Description, "Add step", "Edit a step", "Hide command", "List commands"}
		listOps = append(listOps, "Cancel")
		listOps = append(listOps, "Save&Exit")
		/* 0 = macro description, 1 = new step, 2 = edit step 3 = Cancel 4 = Save&Exit. */
		option, _ := askWithOptions(listOps, false, "What do you want to do ?", "\n ")
		//		nr = 0
		switch option {

		case OPTMACRODESCRIPTION:
			m.Description = askforInput("New description")
		case OPTADDSTEP:
			Message("Add step.", lumber.INFO)
			id := len(steps)
			s := Macrostep{ID: id, Name: "TEST01", Description: "DescriptionTEST01"}
			steps = append(steps, s)
		case OPTEDITSTEP:
			Message("Edit step.", lumber.INFO)
			return
		case OPTHIDECOMMAND:

			answer = askforInput("ID or name of data to hide from the list")
			cFilter = searchCommand(answer)
			if cFilter.Name == "" {
				Message("No data with ID or name "+answer+" found.", lumber.INFO)
			} else {
				if idList, found = mapFilter["ID"]; found {
					idList = append(idList, strconv.Itoa(cFilter.ID))
				} else {
					idList = []string{strconv.Itoa(cFilter.ID)}
				}
				mapFilter["ID"] = idList
			}
		case OPTLISTCOMMANDS:
			t.printFilter(mapFilter)
		case OPTCANCEL:
			Message("Editing canceled without saving data.", lumber.INFO)
			return
		case OPTSAVEEXIT:
			//updateByName(cmd)
			m.Steps = steps
			mAll[index] = m
			if saveData(gDataPath) {
				Message("Data stored for \""+m.Name+"\".", lumber.INFO)

			} else {
				Message("Error while storing data.", lumber.ERROR)

			}
			return
		default:
			Message("Fehler", 0)
		}
	}

	return
}

/* Print array with spaces. */
func printWithSpaces(arg []string) {
	howmany := len(arg)
	arde := make([]string, howmany)
	for i := 1; i < len(arde); i++ {
		arde[i] = "arg" + strconv.Itoa(i)
		for len([]rune(arg[i])) > len([]rune(arde[i])) {
			arde[i] += " "
		}
		fmt.Print(arde[i])
		fmt.Print("  ")
	}
	fmt.Println()
	for i := 1; i < len(arg); i++ {
		fmt.Print(arg[i] + "  ")
	}
	fmt.Println()
	//p("arg: " + strings.Join(arg, " "))
	//	p("arde: " + strings.Join(arde, " "))
}

/*Remove a data. */
//type XSlice []Command
func removeData(args2 string) {
	var i int
	var c Command
	var nr int = len(cAll)
	var suf string
	found := false
	c, i = searchCommandIndex(args2)
	found = c.Name != ""
	if found {
		/* Delete first, ask later... */
		removeByIndex(i)
	} else {
		Message("Data with name or ID \""+args2+"\" does not exist.", lumber.ERROR)
		os.Exit(1)
	}
	text := getShortDesc(c)
	Message(text, lumber.INFO)

	found = askYesNo("Do you really want to remove " + c.Name)
	if !found {
		Message("Removal aborted by user. No data deleted.", lumber.INFO)
		return
	}
	if nr > len(cAll) {
		Message("Data "+args2+" has been removed.", lumber.INFO)

		saveData(gDataPath)
		l := len(cAll)
		if l > 1 {
			suf = "s"
		}
		Message(fmt.Sprintf("Gomando contains now %v data%v.\n", l, suf), lumber.INFO)

	} else {
		Message("Internal error: Could not remove data "+args2, lumber.WARN)
	}
}

//TODO not ready yet.
func removeMany(args []string) {
	//	var removalList []Command
	var answer, str, d, s string
	var idList []string
	//	var toBeContinued bool = true
	var isDeleted, yesNo bool
	var i, countError, countWarnings int
	var c Command
	var nameMap map[string]int
	//	var err error
	const ADJUST = "Adjust deletion list"
	nameMap = make(map[string]int)
	/* Check input arguments. If not there, make automatically a list of all data. */
	if len(args) == 0 {
		d = ""
		ListData(cAll, false)
	} else {
		d = args[0]
	}

	//	fmt.Print(output)
	//for toBeContinued {
	if d != "" {
		s = " (enter to keep \"" + d + "\")"
	}
	answer = askforInputDefault("\nEnter IDs or name of data comma separated"+s+".", d)
	idList = StringToSlice(answer)
	str = ""
	for _, str = range idList {
		isDeleted = false
		c, i = searchCommandIndex(str)
		/* Check for double entries. */
		if _, exists := nameMap[str]; exists {
			Message(str+" is a double in the deletion list.", lumber.WARN)
			countWarnings++
		} else if c.Name == "" {
			//found = true

			//removalList = append(removalList[:i], removalList[i+1:]...)
			Message("An ID with name "+str+" does not exist.", lumber.WARN)
			countWarnings++
		} else {
			isDeleted = removeByIndex(i)
			if isDeleted {
				nameMap[c.Name] = 0
				nameMap[strconv.Itoa(c.ID)] = 0
				Message(getOneliner(c), lumber.INFO)
			} else {
				Message("Error deleting "+str+", index "+strconv.Itoa(i), lumber.ERROR)
				countError++
			}
		}
	}
	if countError > 0 {
		Message("Error while removing data. No data deleted.", lumber.ERROR)
		return
	}
	yesNo = askYesNo("Do you really want to remove the " + strconv.Itoa(len(idList)-countError-countWarnings) + " above listed data")
	if !yesNo {
		Message("Removal aborted by user. No data deleted.", lumber.INFO)
		return
	}

	saveData(gDataPath)
	Message("Deletion of data saved to datastore.", lumber.INFO)
}

/* Removes a macro. */
func removeMacro(args []string) {
	var i int
	var m Macro
	var nr int = len(mAll)
	var suf, name string
	found := false
	if len(args) < 1 {
		name = askforInput("Please enter name or id of macro you want to delete.")
	} else {
		name = args[0]
	}
	m, i = searchMacro(name)
	found = m.Name != ""
	if found {
		/* Delete first, ask later... */
		removeMacroByIndex(i)
	} else {
		Message("Macro with name or ID \""+name+"\" does not exist.", lumber.ERROR)
		os.Exit(1)
	}
	text := getMacroShortDesc(m)
	Message(text, lumber.INFO)

	found = askYesNo("Do you really want to remove macro " + m.Name)
	if !found {
		Message("Removal aborted by user. No data deleted.", lumber.INFO)
		return
	}
	if nr > len(mAll) {
		Message("Data "+name+" has been removed.", lumber.INFO)

		saveData(gDataPath)
		l := len(mAll)
		if l > 1 {
			suf = "s"
		}
		Message(fmt.Sprintf("Gomando contains now %v macro%v.\n", l, suf), lumber.INFO)

	} else {
		Message("Internal error: Could not remove macro "+name, lumber.WARN)
	}
}

/* Get new Command struct without data.
* Name is mandatory to make sure it is never empty.
 */
func getBareCommand(name string) Command {
	if name == "" {
		Message("No name defined. Name is mandatory.", lumber.ERROR)
		os.Exit(1)
	}
	return Command{Name: name, ID: xmlID, Created: time.Now().Format(layout), CreatedBy: gCurrentuser}
}

func addCommand(c Command) {
	cAll = append(cAll, c)
	//c = cAll[len(cAll)-1] //TODO check
	dataIndex[c.Name] = len(cAll) - 1
}
func addMacro(m Macro) {
	if _, found := macroIndex[m.Name]; found {

		Message("Macro or data with name "+m.Name+" exists already.", lumber.ERROR)
		os.Exit(1)
	}
	mAll = append(mAll, m)
	macroIndex[m.Name] = len(mAll) - 1
}

/* Rename renames the name of a date.. */
func rename(oldName string, newName string, doSave bool) {
	var c Command
	var i int
	newName = checkName(newName)
	//	found := false
	c, i = searchCommandIndex(oldName)
	//	for i, c := range cAll {
	if c.Name != "" {
		cAll[i].Name = newName
		cAll[i].Updated = time.Now().Format(layout)
		cAll[i].UpdatedBy = gCurrentuser
		delete(dataIndex, oldName)
		dataIndex[newName] = i
		if doSave {
			Message("Found and renamed "+oldName+" to "+newName+".", lumber.INFO)
			saveData(gDataPath)
		} else {
			Message("Found and renamed "+oldName+" to "+newName+".", lumber.DEBUG)
		}

	} else {
		//	if found == false {
		Message("There is no command stored with name "+oldName+".", lumber.WARN)
	}
}

/* Update existing command by Name, setting updated time and updatedby currentuser. */
func updateByName(cmd Command) bool {
	i, isThere := dataIndex[cmd.Name]
	if isThere {
		cmd.Updated = time.Now().Format(layout)
		cmd.UpdatedBy = gCurrentuser
		cAll[i] = cmd
		return true
	}
	return false
}

/* Same as searchCommandIndex, but returns only Command, no index. */
func searchCommand(search string) Command {
	c, _ := searchCommandIndex(search)
	return c
}

/* Get command by either name or ID of data.
   If not found, it gives back an empty command.
   Check for any attribute = "" like if c.Value= "".
*/
func searchCommandIndex(search string) (c Command, i int) {
	var isNr, isThere, found bool
	var nrID int
	var err error
	if search == "" {
		Message("Search string is empty.", lumber.WARN)
		return Command{}, 0
	}
	i, isThere = dataIndex[search]
	if isThere {
		return cAll[i], i
	}

	/* Now check if search argument is an ID (integer). */
	nrID, err = strconv.Atoi(search)
	isNr = (err == nil && nrID < 1000)
	if isNr == false {
		Message(search+" not found, and not a number for an ID.", lumber.DEBUG)
		return Command{}, 0
	}
	/* Loop all data and check for ID. */
	found = false
	for i, c = range cAll {
		if c.ID == nrID {
			found = true
			break
		}
	}
	if found == false {
		c = Command{}
	}
	return
}

//TODO
func searchMacro(search string) (m Macro, i int) {
	var isNr, isThere, found bool
	var nrID int
	var err error

	i, isThere = macroIndex[search]
	if isThere {
		return mAll[i], i
	}

	/* Now check if search argument is an ID (integer). */
	nrID, err = strconv.Atoi(search)
	isNr = (err == nil && nrID < 1000)
	if isNr == false {
		Message(search+" not found, and not a number for an ID.", lumber.DEBUG)
		return Macro{}, 0
	}
	/* Loop all data and check for ID. */
	found = false
	for i, m = range mAll {
		if m.ID == nrID {
			found = true
			break
		}
	}
	if found == false {
		m = Macro{}
	}
	return
}

func settings() {
	var answer string
	var option int
	var listOptions []string
	listOptions = []string{"Change description for file " + getFilename(gDataPath),
		"Default area for new data storage files",
		"Default area this file",
		"Set area for all data",
		//		"Set learning level",
		"set max column length for lists",
		"Change file format (xml or json)",
		"Show settings for learning mode.",

		"Cancel"}
	option, answer = askWithOptions(listOptions, false, "Gomando Settings", "\n")
	switch option {
	case 0:
		/* XML Description. */
		fmt.Printf("Current file description: '" + xmlRoot.Description + "'.\n")
		answer = askforInput("New description:")
		if answer != "" {
			xmlRoot.Description = answer
			saveCurrentData(true)
		} else {
			fmt.Printf("No change.\n")
		}
	case 1:
		/* Default area for new data xmls. */
		a := xmlConf.DefaultArea
		answer = askforInputDefault("Old area for new data files: \""+a+"\". Enter new area:", a)
		xmlConf.DefaultArea = answer
		saveConfiguration()
	case 2:
		/* Default area for new commands. */
		a := xmlRoot.DefaultArea
		answer = askforInputDefault("Old area of this data xml: \""+a+"\". Enter new area:", a)
		xmlRoot.DefaultArea = answer
		saveCurrentData(true)
	case 3:
		/* Set area for all data. */
		answer := askforInput("New value for all areas of current data:")
		for i, _ := range cAll {
			cAll[i].Area = answer
		}
		Message("Values changed. \n", lumber.INFO)
		saveCurrentData(true)
	case 4:
		/* Field length for lists. */
		answer = askforInput("New field length for screen outputs")
		if nr, err := strconv.Atoi(answer); err == nil {
			xmlConf.Maxfieldlength = nr
			saveConfiguration()
		} else {
			Message(answer+" is not a number.", lumber.ERROR)
			return
		}
	case 5:
		/* Change storage format to xml or json. */
		answer = askforInput("Current file format is " + xmlConf.StorageFormat + ". Enter new format (xml or json).")
		if answer == "xml" || answer == "json" {
			xmlConf.StorageFormat = answer
			saveConfiguration()
			Message("File format is saved ("+answer+").", lumber.INFO)
		} else {
			Message(answer+" is not a valid file format. Must be 'xml' or 'json'.", lumber.ERROR)
			return
		}
	case 6:
		/* Show settings for learning mode. */
		learnSettings()
	case 7:
		return
	default:
		Message("Option not in range.", lumber.ERROR)
	}

}
func learnSettings() {
	var answer string
	var option int
	var listOptions []string
	listOptions = []string{"Show parameterlist from answer (" + boolToString(xmlConf.LearnShowParameter) + ").",
		"Collect up to 3 wrong answers for questions with options (" + boolToString(xmlConf.LearnCollectWrongs) + ").",
		"Cancel"}
	option, answer = askWithOptions(listOptions, false, "Learning Settings", "\n")
	switch option {
	case 0:
		/* Show parameter of answer during learning mode. */
		var yn string
		if xmlConf.LearnShowParameter {
			yn = "Yes"
		} else {
			yn = "No"
		}
		answer = askforInput("Show parameterlist (currently '" + yn + "').")
		xmlConf.LearnShowParameter = strings.ToLower(answer) == "y" || strings.ToLower(answer) == "yes"

		saveConfiguration()
	case 1:
		/* Collect wrong answers for options. */
		var yn string
		if xmlConf.LearnCollectWrongs {
			yn = "Yes"
		} else {
			yn = "No"
		}
		answer = askforInput("Collect wrong answer to include them into option questions (currently '" + yn + "').")
		xmlConf.LearnCollectWrongs = strings.ToLower(answer) == "y" || strings.ToLower(answer) == "yes"

		saveConfiguration()

	case 2:
		return
	default:
		Message("Option not in range.", lumber.ERROR)
	}
}

//func editAll() {
//	for {
//		listOps := []string{"Area ", "Save&Exit"}
//		option, _ := askWithOptions(listOps, false, "\nValues will be changed for all data in file "+gDataPath, "  ")
//		switch option {
//		case 0:
//			answer := askforInput("New value for area (all data):")
//			for i, _ := range cAll {
//				cAll[i].Area = answer
//			}
//			Message("Values changed. \nFile not saved yet.", lumber.INFO)
//		case 1:
//			saveCurrentData(true)
//			return
//		default:
//		}
//	}
//}
