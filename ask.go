package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/jcelliott/lumber"
)

/* Ask for options.
* Input parameter:
* 1. string array with list of options to choose from.
* 2. option to enter new value (true/false)
* 3. title before the options.
* 4. Delimiter like new line or comma.
* Return int number choosen, string value choosen
* Return -1 and new value if new value was choosen.
 */
//TODO: add option to add parameter for a command.
//func askWithOptions(options []string, freeInput bool, title string, delimiter string) (int, string) {
//	return askOptions(options, freeInput, title, delimiter, 0)
//}
func askWithOptions(options []string, freeInput bool, title string, delimiter string) (int, string) {
	var ra int64
	var ok bool
	var b byte
	var count, value int64
	var iv int
	var result string

	if freeInput {
		options = append(options, "New input")
	}

	if title != "" {
		Message(title, 0)
	}
	ra = int64(len(options))
	outString := fmt.Sprintf(delimiter)
	for i, s := range options {
		outString += fmt.Sprintf("%v) %v"+delimiter, i+1, s)
	}
	// outString += fmt.Sprintln()
	Message(outString, 0)
	fmt.Print(Hide())
	for !ok {
		if gOps == OPSUX {
			// modified inputChar func
			// due to its creation of a new ttyl instance during execution we were
			// facing issues while providing input such as backspace and arrow keys
			b = inputChar()
		} else {
			answer := askforInput("Enter number")
			b = byte(answer[0])
		}
		/*
			solution without inputChar func
				answer := askforInput("enter single chan only", false, pr, pw, nil, nil, false)
				b = answer[0]
		*/
		value, _ = strconv.ParseInt(string(b), 0, 64)

		iv = int(value) - 1
		//		Message(fmt.Sprintf("value: %v", b), lumber.DEBUG)

		//	} else {
		//		Message("Undefined yet.", 0, pw)
		//		os.Exit(1)
		//	}
		// TODO suspected error from issue #43 here.
		//		fmt.Printf("value=%v", value)
		ok = value <= ra && value > 0
		if !ok {
			if count > 10 {
				Message("Option not in range.", lumber.ERROR)

				os.Exit(1)
			} else {

				Message("Please choose a value in range of the options.", 0)
				count++
			}
		} else if value == ra && freeInput {
			result = askforInput("Please enter your new value.")
			fmt.Print(Show())
			iv = -1
		} else {
			result = options[iv]
		}

	}
	Message(fmt.Sprintf("Choosen: %v", options[value-1]), lumber.DEBUG)
	fmt.Print(Show())
	return iv, result
}

/* Ask yes or no, taking yes or y as yes, anything else as no. */
func askYesNo(text string) bool {
	if pYes {
		Message("pYes=true", lumber.DEBUG)
		return true
	}
	Message(fmt.Sprint(text+" (y/n) ?"), 0)
	//answer := scanInput()
	//[]string{"Value", "Command", "Web URL", "File", "Macro"}
	//option, _ := askWithOptions(listOps, false, "Choose which value you want to change.", "\n ")
	b := inputChar()
	if string(b) == "y" {
		return true
	}
	return false
}

/* Check name and if input is not empty.
   Argument is proposal that will be kept and returned in case of empty input.
*/
func askName(name string) string {
	var result string
	result = askforInputDefault("Enter the name for the data. Hit enter to keep proposal \""+name+"\". ", "")
	if result == "" {
		return name
	}
	result = checkName(result)
	return result
}

func askforInput(text string) string {
	return askforInputTrim(text, true, "")
}

/* First input text, second is default value. */
func askforInputDefault(text string, d string) string {
	return askforInputTrim(text, true, d)
}
func askforInputTrim(text string, isTrim bool, d string) string {
	Message(text, 0)
	//p("Save now with character s.")
	//	fmt.Print(Show())
	answer := scanInput()
	if answer == "" && d != "" {
		answer = d
		MoveUp(3)
		fmt.Print(d + "\n")
	}
	/* Trim means also that in case of space the value will be empty. */
	if isTrim {
		return strings.Trim(answer, " ")
	}
	//	Message("Answer: "+answer, lumber.INFO)
	return answer
}

// request input from user
func scanInput() (answer string) {

	//	if isTui {
	//		//Message("in tui mode", 0, pw)
	//		returnCh := createTuiIn(ui, grid)
	//		answer = <-returnCh
	//		defer close(returnCh)
	//	} else {

	scanner := bufio.NewScanner(pr)
	//	scanner.Split(bufio.SplitFunc)
	scanner.Scan()
	answer = scanner.Text()
	//}
	return
}

/* Read only single char */
func inputChar() byte {
	var b = make([]byte, 1)
	var answer string

	if gOps == OPSUX || gOps == OPSMAC {
		// disable input buffering
		exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
		// do not display entered characters on the screen
		exec.Command("stty", "-F", "/dev/tty", "-echo").Run()

		// collect input from stdin
		os.Stdin.Read(b)

		// ensable input buffering
		exec.Command("stty", "-F", "/dev/tty", "-cbreak", "min", "0").Run()
		// restore the echoing state when exiting
		exec.Command("stty", "-F", "/dev/tty", "echo").Run()
		return b[0]
	}
	// collects input for windows
	scanner := bufio.NewScanner(pr)
	scanner.Scan()
	answer = scanner.Text()
	return answer[0]
}
