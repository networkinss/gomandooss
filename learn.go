package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/jcelliott/lumber"
)

func learn(args []string) int {
	var ranMax, ranIndex, previous int
	var i, count1, count2, countHidden, confDelay, allItems int
	var listIndex []int
	var answer, v string
	var notFound, doSleep, skipSleep, isDefinedlist, showHelp bool
	var s1 rand.Source
	var c Command
	/* Maximum number of right answers. Above that number item will not be asked for again. */
	var confMaxLevel int
	var countLevel [5]int

	if xmlConf.LearnMaxLevel > 0 {
		confMaxLevel = xmlConf.LearnMaxLevel
	} else {
		confMaxLevel = 3
	}
	/* Check if there are arguments which IDs are to learn. */
	//	ask = true
	if len(args) > 0 {
		listIndex = make([]int, 0)
		all := strings.Join(args, ",")
		ar := strings.Split(all, ",")
		for i, v = range ar {
			if nr, err := strconv.Atoi(v); err == nil { //Check if it is number.
				if nr < len(cAll) && nr >= 0 {
					listIndex = append(listIndex, nr)
				}
			}
		}
		isDefinedlist = len(listIndex) > 0
	}
	showHelp = xmlConf.LearnShowHelp
	confDelay = 3
	Message(xmlRoot.Description, lumber.INFO)

	/* Check how many items are there to learn. */
	for _, c = range cAll {
		if c.Handler == HANDLERMACRO {
			continue
			//		} else if c.Handler == HANDLERFILE && len(c.AlternateValues) == 0 {
			//			continue
		} else if checkFilter(c) {
			allItems++
		}
		if c.Learnhide {
			countHidden++
		}
		countLevel[c.LearnLevel]++

	}
	Info("********************   INFO   ********************")
	Info("Items to learn: " + strconv.Itoa(allItems) + ", excluding " + strconv.Itoa(countHidden) + " hidden items.") //, with maxlevel "+strconv.Itoa(ranMax), lumber.INFO)
	Info("Accomplished level: 0=" + strconv.Itoa(countLevel[0]) + ", 1=" + strconv.Itoa(countLevel[1]) + ", 2=" + strconv.Itoa(countLevel[2]) + ", 3=" + strconv.Itoa(countLevel[3]) + ", 4=" + strconv.Itoa(countLevel[4]))
	Message("Do not show item anymore: press h", lumber.INFO)
	Message("Skip current item (one time): press s", lumber.INFO)
	Message("Save & exit: press q.", lumber.INFO)
	Message("Show parameter list in question: "+strconv.FormatBool(xmlConf.LearnShowParameter), lumber.INFO)
	Message("Collect wrong answers for multiple choice questions: "+strconv.FormatBool(xmlConf.LearnCollectWrongs), lumber.INFO)
	Message("Answers are not case-sensitive.", lumber.INFO)
	Message("Questions will be hidden after 4 correct answers.", lumber.INFO)
	Message("**************************************************\n", lumber.INFO)
	Message("********************   Questions   ***************", lumber.INFO)

	/* Question loop. */
	for {
		count1++
		if count1 > VERSIONLIMITATION {
			Message("Abort due to version limitations.", lumber.WARN)
			saveCurrentData(true)
			return 1
		}
		if isDefinedlist {
			notFound = false
		} else {

			listIndex = make([]int, 0)
			s1 = rand.NewSource(time.Now().UnixNano())
			r1 := rand.New(s1)
			ranMax = r1.Intn(confMaxLevel + 1) //one more to incluse highest level + 0 are 4 levels with high level=3.
			/* Check all learn levels for case there is nothing left. */
			notFound = true
		}
		for notFound {
			for ranMax <= confMaxLevel {
				for i, c = range cAll {
					if c.Handler == HANDLERMACRO {
						continue
						//					} else if c.Handler == HANDLERFILE && len(c.AlternateValues) == 0 {
						//						continue
						/* If there are alternative values, those can be asked for instead of the file value, which is the file itself. */
					}
					if c.LearnLevel <= ranMax && c.Hide == false {
						listIndex = append(listIndex, i)
						notFound = false
						Message("i:"+strconv.Itoa(i)+", name: "+c.Name+", status: "+strconv.Itoa(c.LearnLevel)+", index: "+strconv.Itoa(len(listIndex)), lumber.DEBUG)
					}
					if c.LearnLevel <= confMaxLevel {
						notFound = false
					}
				}
				if len(listIndex) == 0 {
					ranMax++
				} else {
					break
				}
			}
			if len(listIndex) == 0 {
				break
			}
		}
		if notFound {
			fmt.Print("ALL DONE. Nothing left to learn.\n")
			fmt.Print("If you want to restart, call gomando -learnreset. \n")
			saveCurrentData(true)
			return 0
		}
		/* No items would be an error since in that case notFound should be true. */
		if len(listIndex) == 0 {
			Message("Error: no items found.", lumber.ERROR)
			return 1
		}

		/* Check for filter. */
		vorher := len(listIndex)
		listIndex = filteredIndexList(listIndex)
		nachher := len(listIndex)
		Message("Listlength vorher: "+strconv.Itoa(vorher)+", nachher: "+strconv.Itoa(nachher), lumber.DEBUG)
		if nachher == 0 && vorher > 0 {
			Message("No content with current filter.", lumber.INFO)
			return 0
		}

		/* Now chose the item to show. */
		count2 = 0
		for count2 < len(listIndex) {
			count2++
			s1 = rand.NewSource(time.Now().UnixNano())
			r1 := rand.New(s1)
			ranIndex = listIndex[r1.Intn(len(listIndex))]
			c = cAll[ranIndex]
			if c.ID != previous {
				break
			}
		}
		previous = c.ID

		/* Show item meta data. */
		fmt.Print("ID " + strconv.Itoa(c.ID) + ", area '" + c.Area + "', level " + strconv.Itoa(c.LearnLevel) + ".\n")
		//		fmt.Print("Learning level of this " + c.Handler + " is " + strconv.Itoa(c.Learnstatus) + ".\n")
		if showHelp {
			fmt.Print("Use 'q' to save&exit, 's' to skip, 'h' to hide current question and 'help' to show or hide this message.\n")
		}

		/* Generate a parameter list from the answer. */
		p := ""
		if xmlConf.LearnShowParameter {
			p = c.valueOnlyTags()
			if len(p) > 3 {
				p = "\n Parameterlist: " + p
			}
		} else {
			p = ""
		}

		/* Ask */
		if c.Handler == HANDLERFILE { //&& (c.LearnAnswerFormat == "" || c.LearnAnswerFormat == LEARNFORMATFILE) {
			fmt.Print(c.getFileContent())
			//			fmt.Print("\n")
		}

		/* Free answer input format. */
		if c.LearnAnswerFormat == LEARNFORMATINPUT || len(c.WrongValues) == 0 {
			answer = askforInput("'" + c.Description + "'" + p)
		} else if c.LearnAnswerFormat == LEARNFORMATOPTIONS || (c.LearnAnswerFormat == "" && len(c.WrongValues) >= 3) {
			/* Option input format. */
			//Message("canswerformat="+c.LearnAnswerFormat, lumber.INFO)
			var all []string
			all = append(all, c.Value)
			all = append(all, c.WrongValues...)
			//			all = append(all, c.AlternateValues...)
			/* shuffle values randomly */
			rand.Shuffle(len(all), func(i, j int) {
				all[i], all[j] = all[j], all[i]
			})
			all = append(all, "Show answer")
			all = append(all, "Save&Exit")
			i, answer = askWithOptions(all, false, c.Description, "\n")
			if i == len(all)-1 {
				answer = "q"
			} else if i == len(all)-2 {
				answer = ""
			}
			Message("ANSWER: "+answer+", nr: "+its(i), lumber.DEBUG)
		} else {
			Message("ELSE", lumber.DEBUG)
			answer = askforInput("'" + c.Description + "'" + p)
		}

		doSleep = true
		if answer == "q" || answer == "quit" {
			break
			doSleep = false
		} else if answer == "s" || answer == "skip" {
			fmt.Print("Item " + strconv.Itoa(c.ID) + " skipped.\n")
			skipSleep = true
		} else if answer == "h" {
			fmt.Print(c.Name + " is now hidden.\n")
			c.Hide = !c.Hide
		} else {
			if answer == "help" {
				showHelp = !showHelp
				xmlConf.LearnShowHelp = showHelp
				saveConfiguration()
				if showHelp {
					fmt.Print("Use 'q' to save&exit, 's' to skip, 'h' to hide current question and 'help' to show or hide this message.\n")
				}
				answer = askforInput("\"" + c.Description + "\"" + p)
			}
			checkAnswer(answer, &c, confMaxLevel, doSleep)
		}

		cAll[ranIndex] = c
		fmt.Print("\n")

		if doSleep && skipSleep == false {
			time.Sleep(time.Second * (time.Duration(confDelay)))
		}
		skipSleep = false
		if xmlConf.LearnClearScreen {
			if gOps == "windows" {
				RunCapture("cmd /c cls")
			} else if gOps == "linux" {
				RunCaptureNoShell("clear")
			}

		}
	}
	saveCurrentData(true)
	return 0
}

/* Check if answer is correct or not.
   Returns
	0 if correct,
	1 if wrong and
	2 or higher for neutral (like empty answer). */
func checkAnswer(answer string, c *Command, maxLevel int, doSleep bool) int {
	var possible []string

	/* Merge all possible right answers first. */
	possible = c.AlternateValues
	possible = append(possible, c.Value)

	/* Enter with empty value is handled neutral and shows correct answer. */
	if answer == "" {
		if c.Handler == HANDLERFILE {
			if len(c.AlternateValues) == 1 {
				fmt.Print("Correct value is " + c.AlternateValues[0] + "\n")
			} else if len(c.AlternateValues) > 1 {
				fmt.Print("Correct values are " + strings.Join(c.AlternateValues, ", ") + "\n")
			} else {
				fmt.Print("Correct answer is: " + c.Value + "\n")
				//				Message("Warning: no answers defined for this file. Answers for files have to be in alternate values.", lumber.WARN)
				return 0
			}
		} else {
			if len(possible) > 1 {
				fmt.Print("All right answers: " + strings.Join(possible, ", ") + "\n")
			} else {
				fmt.Print("Correct value is: " + c.Value + "\n")
			}

		}
		return 2
	}

	/* Check all possible right answers, including alternate values.. */
	//TODO: fist check all for 100% right answers. After that, check for partial right answers.
	for _, val := range possible {
		if strings.ToUpper(answer) == strings.ToUpper(val) {
			c.LearnLevel += 1
			if c.LearnLevel > maxLevel {
				fmt.Print("\nCONGRATULATIONS!. " + c.Name + " is now marked as learned and will not be asked again.\n")
			} else {
				if answer == val {
					fmt.Print("\nYES, right answer. Learning level is now " + strconv.Itoa(c.LearnLevel) + ".\n")
				} else {
					fmt.Print("\nYES, " + val + " is the right answer. Learning level is now " + strconv.Itoa(c.LearnLevel) + ".\n")
				}

				if len(possible) > 1 {
					fmt.Print("All answers: " + strings.Join(possible, ", ") + "\n")
				}
			}
			return 0

		}
		/* Else check if answer is right without parameters. Check if given answer is longer than one third of correct answer.*/
		if strings.ToUpper(StringRemoveTags(answer)) == strings.ToUpper(StringRemoveTags(val)) && (len(val)/3) > len(answer) {
			fmt.Print("Part of answer is right, but the parameters were wrong. Learning level will be unchanged (" + strconv.Itoa(c.LearnLevel) + ").\n")
			if c.Handler != HANDLERFILE {
				fmt.Print("Full right answer is: '" + val + "'.\n")
			} else {
				fmt.Print("Full right answer is: \n" + strings.Join(c.AlternateValues, "\n") + "\n")
			}
			return 0
		}
		/* Else check if only a part is right. */
		pos1 := strings.Index(val, answer) //given answer is in correct answer
		pos2 := strings.Index(answer, val) //correct answer is in given answer
		//Info("pos1=" + strconv.Itoa(pos1) + ", pos2=" + strconv.Itoa(pos2))
		full := 0
		part := 0
		if pos1 >= 0 {
			part = len(answer)
			full = len(val)
		} else if pos2 >= 0 {
			part = len(val)
			full = len(answer)
		}
		//Info("part=" + strconv.Itoa(part) + ", full=" + strconv.Itoa(full))

		if part > 0 && full > 0 {
			//var percentage int = 0
			percentage := PercentOf(part, full)
			//subanswer := strings.ToUpper(answer[:pos1])
			//Message("DEBUG sub answer: "+subanswer, lumber.DEBUG)
			if percentage > 50 {
				fmt.Print("Answer is partly right (" + strconv.Itoa(int(percentage)) + "%). Because it is more than half right, the learning level will not be decreased (" + strconv.Itoa(c.LearnLevel) + ").\n")
				fmt.Print("Full right answer is: '" + val + "'.\n")
				return 2
			} else {
				fmt.Print("Part is " + strconv.Itoa(int(percentage)) + "% right.\n")

			}

			fmt.Print("Full right answer is: '" + val + "'.\n")

			//}
		}

	}
	fmt.Print("\nWrong answer. \n")
	/* Collect up to 4 wrong answers (for later multiple choice format). */
	wrong := c.WrongValues
	found := false
	for _, v := range wrong {
		if answer == v {
			found = true
		}
	}
	if found == false && xmlConf.LearnCollectWrongs {
		if len(wrong) <= 3 {
			wrong = append(wrong, answer)
		} else {
			var v string
			for i := 0; i < len(wrong)-1; i++ {
				v = wrong[i+1]
				wrong[i] = v
			}
			wrong[len(wrong)-1] = answer
		}
		c.WrongValues = wrong
	}

	fmt.Print("Correct value is: " + c.Value + "\n")
	if c.LearnLevel > 0 {
		c.LearnLevel--
	}
	return 1
}
func PercentOf(part int, total int) float64 {
	return (float64(part) * float64(100)) / float64(total)
}

func learnreset() {
	yesno := askYesNo("This will reset the learning level of all items back to 0. Sure")
	if yesno == false {
		Message("Reset canceled.", lumber.INFO)
		return
	}
	for i, _ := range cAll {
		//		c.Learnstatus = 0
		cAll[i].LearnLevel = 0
	}
	Message("The learning status of all items have been reset to 0.", lumber.INFO)
	saveCurrentData(true)
}

func learnlist() error {
	var t *Table
	header := []string{"ID", "Question", "Answer", "Level"}
	t = new(Table)
	t.New()
	if e := t.setHeader(header); e != nil {
		return e
	}

	for _, c := range cAll {
		d := []string{strconv.Itoa(c.ID), c.Description, c.Value, strconv.Itoa(c.LearnLevel)}
		t.insert(d)
	}
	t.printAll()
	return nil
}
