package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/jcelliott/lumber"
)

func readXMLConfiguration() {
	content, err := ioutil.ReadFile(gConfPath)
	if err == nil {
		err = xml.Unmarshal([]byte(content), &xmlConf)
		if xmlConf.Encrypted == "true" {
			s := stringDecodeBase64(DEFAULTPASSWORD)
			if pPassword == s {
				Message("Configuration is encrypted. Please provide password with parameter -password=<password>.", lumber.ERROR)
				os.Exit(1)
			} else {
				s := stringDecodeBase64(DEFAULTPASSWORD)

				pPassword = pPassword + s[len(pPassword):]
				decryptConfig()
			}
		}
		if xmlConf.Description == FRESHCONF || xmlConf.CreatedBy == "" {
			createConfiguration()
		}
		Message("Using configuration "+gConfPath+".", lumber.DEBUG)

	} else {
		Message("Could not read data from "+gConfPath, lumber.ERROR)
		Message(fmt.Sprintf("%v", err), lumber.ERROR)
	}
}

/* Loads XML data.
Updates the loaded xml data file */
func loadData(xmlPath string, fi os.FileInfo, d string) string {
	var noRead bool
	var dataPath string
	/* Check debug */
	if (isDebug && fi.Name() == "main") && xmlPath == UNDEF {
		dataPath = "/home/amrit/workspace/go/gomando/linux.xml"
	} else if xmlPath != UNDEF && xmlPath != "" {
		/* If -dataxml not defined, function parameter. */
		dataPath = xmlPath
	} else if xmlConf.DefaultDataXML != "" {
		/* If -still not defined, check configuration.xml. */
		dataPath = xmlConf.DefaultDataXML
		Message("Default data xml from configuration="+dataPath, lumber.DEBUG)
	} else if gOps == OPSWIN {
		dataPath = d + gOps + ".xml"
		//TODO make a check for both windos and mac
		//if _, err := os.Stat("/path/to/whatever"); os.IsNotExist(err) {
		//	// path/to/whatever does not exist
		//}
	} else if gOps == OPSUX || gOps == OPSMAC {
		if checkUXWritable(gPathRoot) {
			dataPath = gPathRoot + gOps + ".xml"
		} else if checkUXWritable(d) {
			dataPath = d + gOps + ".xml"
		} else {
			Message("Cannot write data xml file to "+gPathRoot, lumber.ERROR)
			os.Exit(1)
		}
	} else {
		dataPath = d + gOps + ".xml"
	}
	//		if _, err := os.Stat(dataPath); os.IsNotExist(err) && isDefault == false {
	//			dataPath = d + dataPath
	//		}
	/* Check data xml path. */
	if _, err := os.Stat(dataPath); os.IsNotExist(err) {
		if pDataXML == UNDEF && xmlPath == UNDEF && pDefaultXML == UNDEF {
			Message("INFO: Missing data storage xml file "+dataPath+", will be created now.", lumber.INFO)
			xmlRoot = Root{}
			xmlRoot.Description = "Gomando for " + gOps + " command storage."
			xmlRoot.Created = time.Now().Format(layout)
			xmlRoot.CreatedBy = gCurrentuser
			saveData(dataPath)
		} else if pDefaultXML == UNDEF {
			Message("ERROR: Data file "+dataPath+" not found.", lumber.WARN)
			os.Exit(1)
		} else {
			Message("Set new data xml.", lumber.INFO)
			noRead = true
		}
	}
	if noRead == false {
		Message("Load data xml from "+dataPath, lumber.DEBUG)
		readXMLData(dataPath)
	}
	return dataPath
}

/* Read xml data. */
func readXMLData(dataPath string) {
	if CheckPath(dataPath) == false {
		Message("Cannot read file "+dataPath, lumber.ERROR)
		os.Exit(1)
	}
	content, err := ioutil.ReadFile(dataPath)

	if err == nil {
		extractFromContent(content)
		Message("Using data from "+dataPath+".", lumber.DEBUG)
	} else {

		Message(fmt.Sprintf("Could not read data from "+dataPath+"\n %v", err), lumber.ERROR)
		os.Exit(1)
	}
	return
}
func extractFromContent(content []byte) {
	err := xml.Unmarshal([]byte(content), &xmlRoot)
	if err != nil {
		Message(fmt.Sprintf("extractFromContent err: %v", err.Error()), lumber.ERROR)
		os.Exit(1)
	}
	cAll = xmlRoot.Cmd
	mAll = xmlRoot.Macro
	if xmlRoot.Encrypted == "true" {
		s := stringDecodeBase64(DEFAULTPASSWORD)
		if pPassword == s {
			Message("Data are encrypted. Please provide password with parameter -password=<password>.", lumber.ERROR)
			os.Exit(1)
		} else {
			pPassword = pPassword + s[len(pPassword):]
			decryptData()
		}
	}
	/* Create index to have index of each data from slice cAll. */
	createIndex()

	return
}
func saveCurrentData(message bool) bool {
	var r bool
	if r = saveData(gDataPath); r {
		if message {
			Message("Data saved to "+gDataPath+".", lumber.INFO)
		}

	}
	return r
}
func saveData(dataPath string) bool {

	checkIDs()
	// Sorts the command array by time
	if len(cAll) > 1 {
		var tcAll TimeSorter
		tcAll = cAll
		sort.Sort(sort.Reverse(tcAll))
		cAll = tcAll
	}

	xmlRoot.Cmd = cAll
	xmlRoot.Macro = mAll
	if xmlRoot.Encrypted == "true" {
		encryptData()
	}
	return save(xmlRoot, dataPath)
}

/* Create new configuration. */
func createConfiguration() {
	xmlConf = Configuration{Created: time.Now().Format(layout), CreatedBy: gCurrentuser}
	xmlConf.Description = "Gomando configuration, created with gomando version " + version + "."
	xmlConf.Os = gOps
	xmlConf.Encrypted = "none"
	xmlConf.HistoryEnabled = true
	//	xmlConf.DefaultArea = gOps
	xmlConf.Maxfieldlength = 45
	xmlConf.LearnDelay = 3
	xmlConf.LearnMaxLevel = 3
	xmlConf.LearnClearScreen = false
	xmlConf.StorageFormat = "xml"
	xmlConf.LearnCollectWrongs = false
	xmlConf.LearnAcceptAlternate = true
	xmlConf.LearnCaseSensitive = false
	xmlConf.LearnDelay = 3
	xmlConf.LearnShowParameter = true

	if gOps != OPSWIN && checkUXWritable(gPathRoot) == false {
		err := os.MkdirAll(gPathRoot, 0750)
		if checkError(err) == false {
			return
		} else {
			Message("INFO: folder "+gPathRoot+" created.", lumber.INFO)
		}
	}
	saveConfiguration()
}
func saveConfiguration() {
	if xmlConf.Encrypted == "true" {
		encryptConfig()
	}
	Message("Configuration saved.", lumber.INFO)
	save(xmlConf, gConfPath)
}

/* Save to file. */
func save(xmlStruct interface{}, file string) bool {
	var err error
	var output []byte
	format, file := correctFormat(file)
	Message("Saving file: "+file+" in format "+format, lumber.DEBUG)
	if format == "xml" {
		output, err = xml.MarshalIndent(&xmlStruct, " ", " ")
	} else {
		output, err = json.MarshalIndent(&xmlStruct, " ", " ")
	}
	if checkError(err) {
		return writeFile(file, output, "0644")
	}
	Message("XML format not valid for "+file, lumber.ERROR)
	return false
}

//TODO like JSON
func saveOther(xmlStruct interface{}, file string) bool {
	Message("Saving file: "+file, lumber.DEBUG)
	output, err := xml.MarshalIndent(&xmlStruct, " ", " ")
	if checkError(err) {
		return writeFile(file, output, "0644")
	}
	Message("XML format not valid for "+file, lumber.ERROR)
	return false
}

/* Export into pPath defined file with defined filter in pArea and pFilter. */
func exportXML() {
	found := false
	isLimited := false
	var resultXML Root
	var resultCmd []Command = []Command{}
	resultXML.Description = askforInput("Please enter description for the exported data.")
	/* Build output data. */
	//Info("Amount of data is limited to " + pLimit)
	from, till := GetFromTill(pLimit)
	isLimited = from > 0 || till > 0
	if from > till {
		Message("Limit values are wrong (from > till) and ignored.", lumber.WARN)
		isLimited = false
		from = 0
		till = 0
	}
	if isLimited {
		Info("Limit: from " + strconv.Itoa(from) + " till " + strconv.Itoa(till) + ".")
	}

	for counter, c := range cAll {
		if checkFilter(c) {
			if counter >= till && isLimited {
				break
			}
			if counter >= from {
				resultCmd = append(resultCmd, c)
				found = true
			}
		}
	}
	if found == false {
		Message("There was no command to export with defined filters.", 0)
		if askYesNo("Save (empty) export anyway ?") {
			found = true
		}
	}

	if found {
		resultXML.Cmd = resultCmd
		if strings.ToUpper(pPath[len(pPath)-4:]) != ".XML" {
			pPath = pPath + ".xml"
		}
		if save(resultXML, pPath) {
			Message(fmt.Sprintf("Exported %v data into %v.", len(resultCmd), pPath), lumber.INFO)

		} else {
			Message("Error during save of xml "+pPath, lumber.ERROR)

			os.Exit(1)
		}
	} else {
		Message("Nothing to export, no file saved.", lumber.INFO)

	}

}

/* Import pPath defined file with defined filter in pArea and pFilter
into current data xml.
*/
func importXML() {
	found := false
	var count int
	var resultCmd []Command = []Command{}
	var originalCmd []Command = make([]Command, len(cAll)) //backupData contains now standard data xml (linux.xml).
	var originalDataFile string
	var originalRoot Root = xmlRoot
	copy(originalCmd, cAll)
	originalDataFile = gDataPath
	xmlRoot.Cmd = []Command{} //Make xmlRoot.Cmd command list empty for xml to import.
	loadData(pPath, fi, "")

	for _, c := range xmlRoot.Cmd {
		if checkFilter(c) {
			resultCmd = append(resultCmd, c)
			Message("Importdata: "+c.Name, lumber.DEBUG)
			count++
		}
	}
	found = count > 0
	//resultCmd has now imported and filtered data.
	if found == false {
		Message("There was no command to import with defined filters.", 0)
		if askYesNo("Import anyway ?") {
			found = true
		}
	}
	Message("Found "+strconv.Itoa(count)+" data to import.", lumber.INFO)
	if found {
		cAll = append(originalCmd, resultCmd...)
		gDataPath = originalDataFile
		checkIDs()
		originalRoot.Cmd = cAll
		Message("original, datapath: "+originalDataFile+", "+gDataPath, lumber.DEBUG)
		if save(originalRoot, gDataPath) {
			Message(fmt.Sprintf("Imported %v data into %v.", len(resultCmd), gDataPath), lumber.INFO)

		} else {
			Message("Error during save of xml "+gDataPath, lumber.ERROR)
			os.Exit(1)
		}
	} else {
		Message("Nothing to import, no file saved.", lumber.INFO)

	}

}
func newXmlstore(args []string, folder string) {
	var newPath, name string
	var checkList []string
	var answer bool
	if len(args) < 1 {
		for name == "" {
			name = askforInput("Please enter name for the new xml storage file.")
		}
	} else {
		name = args[0]
	}
	if len(name) <= 4 {
		name = name + ".xml"
	} else if len(name) > 4 && name[len(name)-4:] != ".xml" {
		name = name + ".xml"
	}
	checkList = getFolderxmls(folder)
	for _, str := range checkList {
		if str == name {
			Message("Error: File "+str+" exists already in "+folder+". Please choose another name.", lumber.ERROR)
			return
		}
	}
	newPath = folder + name
	answer = askYesNo("Do you want to make " + name + " the new default")
	if answer {
		xmlConf.DefaultDataXML = newPath
		saveConfiguration()
	}

	xmlRoot = Root{}
	xmlRoot.Description = "Gomando for " + gOps + " command storage."
	xmlRoot.Created = time.Now().Format(layout)
	xmlRoot.CreatedBy = gCurrentuser

	xmlRoot.DefaultArea = xmlConf.DefaultArea
	cAll = []Command{} //make a new xml and remove all loaded data.
	saveData(newPath)
	Message("New storage xml file \""+name+"\" created.", lumber.INFO)
	Message("Path of new xml storage="+newPath, lumber.DEBUG)

}
func setXmldefault(path string) {
	path = checkXMLEnding(path)
	if path == "" {
		Message("Please define path to xml (-setdefault=<xmlfile>).", lumber.ERROR)
		os.Exit(1)
	}
	if CheckPath(path) == false {
		path = gPathRoot + path
		Message("Trying default xml: "+pDefaultXML, lumber.DEBUG)
	}
	if CheckPath(path) == false {
		path = binDir + path
		Message("Trying default xml: "+path, lumber.DEBUG)
	}
	if CheckPath(path) == false {
		Message("File "+path+" does not exist.", lumber.ERROR)
		os.Exit(1)
	}
	xmlConf.DefaultDataXML = path
	saveConfiguration()

}
