package main

import (
	"encoding/xml"
	"strings"
	"time"
)

type Root struct {
	XMLName      xml.Name  `xml:"gomando"`
	Description  string    `xml:"description"`
	AltLogopath  string    `xml:"altlogopath"`
	DefaultArea  string    `xml:"defaultarea"`
	Created      string    `xml:"createdat"`
	CreatedBy    string    `xml:"createdby"`
	Encrypted    string    `xml:"encrypted"`
	Cmd          []Command `xml:"cmd"`
	Macro        []Macro   `xml:"macro"`
	Explanations []string  `xml:"explanations"`
}

type Parameter struct {
	Argtype     string `xml:"type,attr"`
	Value       string `xml:"param"`
	Description string `xml:"description"`
}

// Configuration collects and stores the information regarding application setup
// in xml format. the default file is Configuration.xml
type Configuration struct {
	XMLName           xml.Name `xml:"configuration"`
	Description       string   `xml:"description"`
	Title             string   `xml:"title"`
	CreatedBy         string   `xml:"createdby"`
	Created           string   `xml:"createdat"`
	Updated           string   `xml:"updatedat"`
	Os                string   `xml:"os"`
	DefaultDataXML    string   `xml:"defaultdataxml"`
	DefaultArea       string   `xml:"defaultarea"`
	StorageFormat     string   `xml:"storageformat"`
	Mailaddress       string   `xml:"mailaddress"`
	MailSMTP          string   `xml:"mailsmtp"`
	Mailport          string   `xml:"mailport"`
	Mailloginname     string   `xml:"mailloginname"`
	Mailloginpassword string   `xml:"mailloginpassword"`
	Encrypted         string   `xml:"encrypted"`
	Loglevel          string   `xml:"loglevel"`
	Logoutput         string   `xml:"logoutput"`
	Logpath           string   `xml:"logpath"`
	Logofile          string   `xml:"logofile"`
	WebIPCheck        string   `xml:"webipcheck"`
	HistoryEnabled    bool     `xml:"historyEnabled"`
	Maxfieldlength    int      `xml:"maxfieldlength"`

	LearnClearScreen     bool `xml:"learnclearscreen"`
	LearnMaxLevel        int  `xml:"learnmaxlevel"`
	LearnDelay           int  `xml:"learndelay"`
	LearnDifficulty      int  `xml:"learndifficulty"`
	LearnCollectWrongs   bool `xml:"learncollectwrongs"`
	LearnAcceptAlternate bool `xml:"learnacceptalternate"`
	LearnCaseSensitive   bool `xml:"learncasesensitive"`
	LearnShowParameter   bool `xml:"learnparameterlist"`
	LearnShowHelp        bool `xml:"learnshowhelp"`
	//	Name string `json:"name" xml:"name"`

	WikiUsername string `xml:"wikiusername"`
	WikiPassword string `xml:"wikipassword"`
	WikiLoginURL string `xml:"wikiloginurl" ,omitempty"`
	WikiDomain   string `xml:"WikiDomain" ,omitempty"`
}

//TODO change field names like Handler to Type.
type Command struct {
	ID                int          `xml:"id,attr"`
	Name              string       `xml:"name"`
	Value             string       `xml:"value"`
	AlternateValues   []string     `xml:"alternatevalues,omitempty"`
	Description       string       `xml:"description"`
	LongDescription   string       `xml:"longdescription"`
	File              string       `xml:"file,omitempty"`
	FilePermission    string       `xml:"filepermission,omitempty"`
	FilePath          string       `xml:"filepath,omitempty"`
	Properties        []Properties `xml:"properties,omitempty"`
	Tags              string       `xml:"tags"`
	P                 []Parameter  `xml:"parameter"`
	Handler           string       `xml:"handler"`
	Area              string       `xml:"area"`
	Created           string       `xml:"createdat"`
	CreatedBy         string       `xml:"createdby"`
	Updated           string       `xml:"updatedat"`
	UpdatedBy         string       `xml:"updatedby"`
	Blocker           Blocker      `xml:"blocker,omitempty"`
	LastUsed          time.Time    `xml:"lastUsed"`
	Status            string       `xml:"status"`
	Hide              bool         `xml:"hide"`
	Macroposition     int          `xml:"makroposition"`
	LearnLevel        int          `xml:"learnlevel"`
	Learnhide         bool         `xml:"learnhide"`
	WrongValues       []string     `xml:"wrongvalues,omitempty"`
	LearnAnswerFormat string       `xml:"learnanswerformat"` //can be single,list or multi.TODO: add file content.
	LearnNumberValues int          `xml:"learnnumbervalues"` //how many right (alternate) values to enter.
}
type Macro struct {
	ID          int         `xml:"id,attr"`
	Name        string      `xml:"name"`
	Description string      `xml:"description"`
	Steps       []Macrostep `xml:"macrostep"`
}
type Macrostep struct {
	ID          int    `xml:"id,attr"`
	Name        string `xml:"name"`
	Description string `xml:"description"`
	DataID      string `xml:"dataid"`
	Position    int    `xml:"position"`
}

type Properties struct {
	Key   string `xml:"key,omitempty"`
	Value string `xml:"value,omitempty"`
}

/* Comma separated int values stored as strings. */
type Blocker struct {
	Hours         string `xml:"hours,omitempty"`
	Weekday       string `xml:"weekdays,omitempty"`
	Monthday      string `xml:"monthdays,omitempty"`
	Months        string `xml:"months,omitempty"`
	Year          string `xml:"years,omitempty"`
	Password      string `xml:"password,omitempty"`
	WhitelistUser string `xml:"whitelistuser,omitempty"`
	BlacklistUser string `xml:"blacklistuser,omitempty"`
}

func (cmd *Command) getShortValue(maxlength int) (data string) {
	//	if cmd.Handler == HANDLERFILE {
	//		by, ok := decodeBase64(cmd.Value)
	//		if ok {
	//			data = string(by)
	//			data = strings.Replace(data, "\n", ", ", -1)
	//		} else {
	//			Message("Error in file data. Not base64 encoded.", 3)
	//			data = "ERROR"
	//		}
	//	} else {
	data = cmd.Value
	//	}
	if len(data) > maxlength {
		data = data[:maxlength] + "..."
	}

	return
}

func (cmd *Command) valueOnlyTags() string {
	var pos1, pos2, i, count int
	var value, result string
	value = cmd.Value
	count = strings.Count(value, "<")
	if count < 0 {
		return ""
	}

	for i = 0; i <= count; i++ {
		pos1 = strings.Index(value, "<")
		if pos1 > 0 {
			pos2 = strings.Index(value, ">") + 1
			if pos2 > pos1 {
				sval := value[pos1:pos2]

				value = strings.Replace(value, sval, "", 1)
				result += sval
				pos1 = pos2 + 1
			}
		}
	}
	return strings.Trim(result, " ")
}

/* Returns
   1. String value of the file content.
   2. Path to file.
*/
func (cmd *Command) getFileContent() string {
	if cmd.Handler != HANDLERFILE {
		return "ERROR: No file data."
	}
	s := stringDecodeBase64(cmd.File)
	//	propMap := make(map[string]string)
	//	for _, prop := range cmd.Properties {
	//		propMap[prop.Key] = prop.Value
	//	}
	//	msg = fmt.Sprintf("\nFile content:\n%v\nFile path:\n%v\n", s, propMap["path"])
	return s //, cmd.FilePath // propMap["path"]
}

/*
	makePropMap takes a command and returns a map of its properties.
*/
func (cmd *Command) getPropMap() (propMap map[string]string) {
	propMap = make(map[string]string)
	for _, prop := range cmd.Properties {
		propMap[prop.Key] = prop.Value
	}
	return
}
func (cmd *Command) getProperty(prop string) string {
	var propMap map[string]string
	propMap = make(map[string]string)
	for _, prop := range cmd.Properties {
		propMap[prop.Key] = prop.Value
	}
	return propMap[prop]
}
