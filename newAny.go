package main

import (
	"os"
	//	"strings"

	"github.com/jcelliott/lumber"
)

// newAny launched when -new flag is called
//TODO: add option to enter parameter list.
func newAny(args []string) {
	var ic int
	var data, handler, name string
	var nc Command
	Message("New data entry (ctrl+c to cancel).", 0)
	if len(args) > 0 {
		name = args[0]
	} else {
		name = ""
	}
	if len(hrsl) != len(mrsl) {
		Message("Internal problem with handler slices.", lumber.ERROR)
		os.Exit(3)
	}

	/* Ask for data type. */
	ic, _ = askWithOptions(hrsl, false, "Choose type of data.", "   ")
	handler = mrsl[ic]

	/* Enter the main value. */
	text := mrsl[ic]
	if text == HANDLERFILE {
		text += " path"
	} else if text == HANDLERWEB {
		text += " URL"
	} else if handler == HANDLERMACRO {
		/* Macro */
		newMacro([]string{name})
		return
	} else if handler == HANDLERVALUE {
		text += ", any value or an answer to a question for learning"
	}
	/* All other */
	text = "Enter the " + text
	if name == "" {
		text += "."
	} else {
		text += " (or enter to keep '" + name + "')."
	}
	data = askforInputTrim(text, true, name)

	/* Get a default command variable. Fields like createdby and updated are set. */
	nc = quickStore(data, false, handler, data, false)

	nc.Description = askforInputDefault("Enter description or a question for learning (enter to keep \""+nc.Description+"\").", nc.Description)
	/* */
	if name == "" {
		if len(nc.Description) > 6 {
			name = nc.Description[:6]
		} else {
			name = nc.Description
		}
		//		name = askName(nc.Name) //deprecated
	}
	name = checkName(name)

	//	/* Rename in cAll and then here nc.*/
	//	rename(nc.Name, name, false)
	nc.Name = name
	//nc := newBasicCommand(name)
	//nc.Value = data
	/* File */
	if handler == HANDLERFILE {
		path := checkFilepath(data)
		nc = newFile(path, nc.Description, nc.Name, nc.Tags, 2)
		//		return
	}

	//	var pos1, pos2 int
	//	var val string
	//	pos1 = strings.Index(nc.Value, "<")
	//	if len(nc.Value) >= pos1+1 && pos1 >= 0 {
	//		pos2 = strings.Index(nc.Value[pos1+1:], ">")
	//		if pos2 >= 0 {
	//			//			val = nc.Value[pos1+1 : pos2+pos1+1]
	//			//			fmt.Fprintf(pw, "Found value: %s\n", val)
	//		}
	//	}

	addCommand(nc)
	ok := editData([]string{nc.Name})
	if ok {
		Message("To retrieve or execute stored data, type \"gomando "+its(nc.ID)+"\".", lumber.INFO)
	} else {
		Message("Error while storing data.", lumber.ERROR)
	}

	return
}

/* Get basic Command struct with main values askek for:
* Name
* Description
* Tags
 */
func newBasicCommand(name string) Command {
	var c Command
	if name == "" {
		name = askforInput("Please enter name.")
	}

	c = getBareCommand(name)
	// TODO not sure trim should be false
	c.Description = askforInput("Please enter a description.")
	//c.Tags = askforInput("Please enter tags (separate with comma)")
	c.Area = gOps
	return c
}
func newBasicMacro(name string) (m Macro) {
	m.ID = len(mAll)
	if name == "" {
		name = askforInput("Please enter a macro name.")
	}
	m.Name = name
	m.Description = askforInput("Please enter a macro description.")
	return
}

/* Macro with details for execution of commands. */
func newMacro(args []string) {
	var name string
	var m Macro

	if len(args) > 0 {
		name = args[0]
	}
	if name == "" {
		name = askName(name)
	}
	name = checkName(name)
	Message("Starting to create a new macro: "+name+".", lumber.DEBUG)
	m = newBasicMacro(name)

	addMacro(m)
	editMacro([]string{m.Name})

}
