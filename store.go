package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/jcelliott/lumber"
)

// Quick store input into cAll as new data (value or web url).
// Tags and name are guessed. Description is token2 if argument is empty.
func quickStore(input string, doSave bool, handler string, desc string, doAdd bool) Command {
	//	linenr := 1
	var token1, token2 string
	var rest string
	var tags string
	var isWeb bool
	var value = input

	f := func(r rune) bool {
		return r < 'A' || r > 'z'
	}
	ff := func(r rune) bool {
		return r >= 'A' && r <= 'z'
	}
	/* Check if it is an URL. */
	isWeb = CiSearch(input, "http://") || CiSearch(input, "www.") || CiSearch(input, "https://")
	if isWeb {
		tags += "web,http"
		if CiSearch(input, "https://") {
			tags += ",https"
		}
		input = strings.Replace(input, "https://", "", -1)
		input = strings.Replace(input, "http://", "", -1)
		input = strings.Replace(input, "www.", "", -1)
	}
	/* pos is first special character. */
	pos := strings.IndexFunc(input, f)
	for pos == 0 { //case first characters are special.
		if pos < len(input)+1 {
			input = input[pos+1:]
			pos = strings.IndexFunc(input, f)
		} else {
			pos = len(input) - 1
		}
	}
	if pos != -1 {
		b := input[pos]
		if b != 10 && pos < len(input) {

			token1 = input[:pos]
			rest = input[pos+1:]

			pos = strings.IndexFunc(rest, ff)
			//			fmt.Printf("rest,pos,token1: %v, %v, %v\n", rest, pos, token1)
			if pos >= 0 {
				//					fmt.Printf("pos>1: %v\n", pos)
				pos2 := strings.IndexFunc(rest[pos+1:], f) + pos
				//					fmt.Printf("pos2: %v\n", pos2)
				if pos2 < 0 {
					pos2 = len(rest)
				}
				if pos2 < len(rest) {
					//					fmt.Printf("pos: %v, pos2: %v, len: %v", pos, pos2, len(rest))

					token2 = rest[pos : pos2+1]
				} else {
					token2 = rest[pos:]
				}

			} else {
				token2 = rest
			}

		} else {
			token1 = input
			token2 = input
		}

	} else {
		token1 = input
		token2 = input
	}
	name := checkName(token1)
	d := getBareCommand(name)
	if token1 != "" && token1 != token2 {
		tags = token1 + "," + token2
	} else {
		tags = token1
	}

	d.Tags = tags
	d.Handler = handler
	d.Area = gArea
	d.Value = value
	if desc == "" {
		desc = token2
	}
	d.Description = desc
	if doAdd {
		addCommand(d)
	}
	if doSave {
		saveData(gDataPath)
		Message("Input registered with value \""+d.Value+"\" and description \""+d.Description+"\"", lumber.INFO)
		Message("Recall "+d.Handler+" with  \"gomando "+its(d.ID)+"\"\".", lumber.INFO)
	}
	Message(fmt.Sprintf("ID: %v\n", d.ID), lumber.DEBUG)
	return d
}

/* Quick store value,command or web url with array arguments. */
func storeLine(handler string, args string) {
	//	done = make(chan int)
	//	go func(handler string, args []string, pw *os.File) {
	//line := os.Args[2:]
	//sLine := strings.Join(args, " ")
	var desc string
	desc = args

	quickStore(args, true, handler, desc, true)
	//	done <- 0
	//	}(handler, args, pw)
	return
}

/* Quick store for files with
* filename as name and as Description
* Suffix as tags.
* and do the rest with func newFile
 */
func quickFile(args []string) {
	var file, path, suffix string
	if len(args) < 1 {
		path = askforInput("Please enter file path.")
		if path == "" {
			Message("Path is empty.", lumber.ERROR)
			os.Exit(1)
		}
	} else {
		path = args[0]
	}

	path = checkFilepath(path)
	pos := strings.LastIndex(path, string(os.PathSeparator))
	file = path[pos+1:]

	pos = strings.LastIndex(file, ".")
	if pos > 1 {
		suffix = file[pos+1:] + "," + file
	} else {
		suffix = file
	}
	newFile(path, file, file, suffix, 3)

	return
}

/* Write file
* file path to the file
* content bytes of content of the file
* permission in form 0644
* return true if successful. */
func writeFile(file string, content []byte, permission string) bool {
	if permission == "" {
		Message("No valid file permission set.", lumber.DEBUG)
		permission = "0644"
	} else if len(permission) == 3 {
		permission = "0" + permission
	}
	modi, _ := strconv.ParseUint(permission, 8, 32)
	//	if ops == "linux" && checkUXWritable(file) == false {
	//		Message("Cannot write into file "+file+".", lumber.ERROR)
	//		return false
	//	} else {
	err := ioutil.WriteFile(file, content, os.FileMode(uint32(modi)))
	if checkError(err) {
		Message("Content written into "+file+".", lumber.DEBUG)
		return true
	}
	//}

	//Message("Error during write operation for file "+file+".", lumber.ERROR)
	return false
}
