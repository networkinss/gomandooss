package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/jcelliott/lumber"
)

/* Execute data with given name or id. */
func runCommandFromName(macroName string) int {
	var result int

	found, c, _ := searchComm(macroName)
	if !found {
		result = 1
		if askYesNo("Wrong id or name, cannot be executed. Create new data") {
			newAny([]string{c.Name})
			result = 0
		}
		os.Exit(result)
	}
	/*
		if ui != nil {
			ui.Quit()
		}
	*/

	// _, cAll = RunMacro(macroName, c, log, os.Stdin, os.Stdout)
	// result, cAll = RunMacro(macroName, c, log, os.Stdin, os.Stdout)
	result, cAll = RunMacro(macroName, c, log)
	//	done <- 1
	os.Exit(result)
	//	}()
	return result
}

// RunMacro Run or show data.
// Runs a single comand with RunCapture look in case yaourt for eg
func RunMacro(search string, c Command, log lumber.Logger) (int, []Command) {
	var strAr []string
	//	var check bool

	/*
			for _, c = range cAll {
				if c.Name == search {
					found = true
					//			p("Command found: " + c.Name + ", " + c.Description)
					break
				}
			}

		if found == false {
			Message("Data with name " + search + " not found.")
			if askYesNo("Create new data") {
				newAny(search)
				os.Exit(0)
			} else {
				return 1
			}
		}
	*/

	/* Read all properties and store them in a key value map. */
	//	propMap := c.getPropMap() //makePropMap(c)

	f, err := exec.LookPath(c.Value)
	if err != nil {
		//		p("Could not execute " + com)
	} else {
		_ = f
		//		p("Command found in path: " + f)
	}
	/* Check first blocking conditions. */
	if checkIsBlocked(c.Blocker) {
		Message("Execution is blocked due to the current configuration.", lumber.ERROR)
		return 2, cAll
		//os.Exit(2)
	}

	// Sets LastUsed time and updates cAll copy in this func
	// need to return this to the main.go so as to avoid errors
	cAll = setLastUsedComm(c, cAll)

	/*
		// Only show the value stored.
		if show {
			if c.Handler == HANDLERVALUE {
				Message("c.Value stored:\n" + com)
			} else if c.Handler == HANDLERWEB {
				Message("Web URL:\n" + com)
			} else if c.Handler == HANDLERFILE {
				by := decodeBase64(com)
				Message("Content of file:\n" + string(by))
				Message("File path:\n" + propMap["path"])
			} else {
				Message("Stored data:\n" + com + strings.Join(strAr, " "))
			}
			r = 0
		} else {
	*/

	if c.Handler == HANDLERVALUE {
		Message("Handlervalue", lumber.DEBUG)
		Message(c.Value, 0)
	}

	if c.Handler == HANDLERCMD {
		c.LastUsed = time.Now()
		/* Get first all parameter for placeholders defined with <parameter>.*/
		val := getParameter(c.Value)
		output := RunCapture(val)
		Message(output, 0)
	} else if c.Handler == HANDLERFILE {
		by, ok := decodeBase64(c.File)
		if ok == false {
			Message("Error in file data. Not base64 encoded.", lumber.ERROR)
			return 1, []Command{}
		}
		//		perm := propMap["permission"]
		writeFile(c.FilePath, by, c.FilePermission)
		Message("File written to: "+c.FilePath, 0)
	} else if c.Handler == HANDLERMACRO {
		cmd := exec.Command(c.Value)
		strAr = buildParameter(c.P)
		cmd.Args = append(cmd.Args, strAr...)

		// Proxy the cmd outputs and input to system defaults
		cmd.Stdout = pw
		cmd.Stdin = pr
		cmd.Stderr = pw

		if err := cmd.Start(); err != nil {
			log.Fatal(err.Error())
		}
		if err := cmd.Wait(); err != nil {
			log.Fatal(err.Error())
		}
	} else if c.Handler == HANDLERWEB {
		Message("Opening url in browser "+c.Value+"\n", 0)
		openURL(c.Value)
	}

	return 0, cAll
}

// RunCapture splits a command string into its components
// runs the command found at [0] and attaches the rest of the string as arguments
// it proxies the os stdout, stderr and stdin to the cmd stdout, stderr and stdin
// known exception of shell buildin command - cd
func RunCaptureNoShell(command string) {
	isNoshell = true
	RunCapture(command)
}
func RunCapture(command string) (output string) {
	output = "Gomando is done"
	args := strings.Split(command, " ")
	bash := []string{"/bin/bash", "-c"}
	if gOps != OPSWIN && !isNoshell {
		args = append(bash, command)
	}
	fmt.Println(`Gomando is executing command `, args)
	cmd := exec.Command(args[0], args[1:]...)

	// Proxy stdin stdout and stderr to os std values
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// Start command execution
	err := cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "cmd.Start() failed with", err)
		os.Exit(1)
		// Lumber loging package causing unknown runtime error
		//log.Fatal("cmd.Start() failed with ", err)
	}

	// Wait till command finishes or experences error
	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Cmd.wait() failed with", err)
		os.Exit(1)
		// Lumber loging package causing unknown runtime error
		// log.Fatal("cmd.Run() failed with", err)
	}

	return
}

/* Get all runtime parameter like <param1>, <param2>. */
func getParameter(value string) string {
	var pos1, pos2, i, count int
	count = strings.Count(value, "<")
	if count <= 0 {
		return value
	}
	countstr := strconv.Itoa(count)
	Message("Please enter "+countstr+" parameter for command "+value, lumber.INFO)
	for i = 1; i <= count; i++ {
		pos1 = strings.Index(value, "<")
		if pos1 > 0 {
			pos2 = strings.Index(value, ">") + 1
			if pos2 > pos1 {
				sval := value[pos1+1 : pos2-1]
				Message("DEBUG Substring: "+sval, lumber.DEBUG)
				sval = askforInput(strconv.Itoa(i) + ": " + sval)
				value = value[:pos1] + sval + value[pos2:]
			}
		}
	}
	Message("Executing command: "+value, lumber.INFO)
	return value
}

/* Open a url in browser. */
func openURL(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case OPSWIN:
		cmd = "cmd"
		args = []string{"/c", "start"}
	case OPSMAC:
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

// RunExit TODO
//func RunExit(command string) int {
//	args := strings.Split(command, " ")
//	cmd := exec.Command(args[0], args[1:]...)
//	cmd.Stdout = os.Stdout
//	cmd.Stderr = os.Stderr
//	if err := cmd.Run(); err != nil {
//		if exiterr, ok := err.(*exec.ExitError); ok {
//			return exiterr.Sys().(syscall.WaitStatus).ExitStatus()
//		}
//		fmt.Printf("command failed: %s\n", command)
//		panic(err)
//	}
//	return 0
//}

// RunMulti TODO
//func RunMulti(command string, args ...string) {
//	var cmd *exec.Cmd
//	if len(args) == 0 {
//		//fmt.Printf("run: %s\n", command)
//		args := strings.Split(command, " ")
//		cmd = exec.Command(args[0], args[1:]...)
//	} else {
//		fmt.Printf("run: %s %s\n", command, strings.Join(args, " "))
//		cmd = exec.Command(command, args...)
//	}
//	cmd.Stdout = os.Stdout
//	cmd.Stderr = os.Stderr
//	if err := cmd.Run(); err != nil {
//		fmt.Printf("command failed: %s\n", command)
//		panic(err)
//	}
//}

// RunSingle TODO
//func RunSingle(command string, pw *os.File) {
//	fmt.Printf("run: %s\n", command)
//	args := strings.Split(command, " ")
//	cmd := exec.Command(args[0], args[1:]...)
//	cmd.Stdout = os.Stdout
//	cmd.Stderr = os.Stderr
//	cmd.Stdin = os.Stdin
//	if err := cmd.Run(); err != nil {
//		fmt.Printf("command failed: %s\n", command)
//		checkError(err)
//	}
//}
