package main

import (
	"io/ioutil"
	"net/http"
	"net/smtp"
	"os"
	"strconv"

	"github.com/jcelliott/lumber"
	//"github.com/elazarl/goproxy"
)

/* Start a file or proxy server. */
func runServer() {
	if pPort == UNDEF {
		pPort = "8080"
	}
	if _, err := strconv.Atoi(pPort); err != nil {
		Message("Given port "+pPort+" is not a number. Please provide number for port or leave empty to default 8080.", lumber.ERROR)
		pPort = "8080"
	}

	if pServer == "file" {
		Message("Starting "+pServer+" server on port "+pPort+".", 0)
		startFileserver(pPort)
	} else if pServer == "proxy" {
		Message("Starting "+pServer+" server on port "+pPort+".", 0)
		if pProxy != UNDEF {
			Message("Additional parameter set: "+pProxy, 0)
		}

		startproxy()

		// proxy := goproxy.NewProxyHttpServer()
		// proxy.Verbose = true
		// err := http.ListenAndServe(":"+pPort, proxy)
		// if err != nil {
		// 	Message(err.Error(), lumber.ERROR)
		// }
	} else {
		Message("Server type "+pServer+" is unknown. Exiting program.", lumber.WARN)
		os.Exit(1)
	}

}

func startFileserver(port string) {
	http.HandleFunc("/", fileServer)
	http.ListenAndServe(":"+port, http.FileServer(http.Dir("")))
}

func fileServer(w http.ResponseWriter, req *http.Request) {
	var text string
	text = req.URL.Path[1:]
	Message("URL request: "+text, lumber.DEBUG)
	data, err := ioutil.ReadFile(string(text))
	if err == nil {
		w.Write(data)
	} else {
		w.Write([]byte("FEHLER"))
		//		log.Printf("Len: %s\n", len(text))
		//		log.Printf("data: %s", string(data))
	}

}

func mail(body string) {
	// Set up authentication information.
	auth := smtp.PlainAuth("", xmlConf.Mailloginname, xmlConf.Mailloginpassword, xmlConf.MailSMTP)

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{"info@inss.ch"}
	msg := []byte("To: " + xmlConf.Mailaddress + "\r\n" +
		"Subject: Automated Don Message\r\n" +
		"\r\n" + body + "\r\n")
	err := smtp.SendMail(xmlConf.MailSMTP+":"+xmlConf.Mailport, auth, xmlConf.Mailaddress, to, msg)
	if checkError(err) {
		Message("Mail sent to "+xmlConf.Mailaddress+".", lumber.INFO)

	}
}
