// +build !windows

package main

import (
	"github.com/jcelliott/lumber"
	"golang.org/x/sys/unix"
)

/* Check if path exists and if it is writable (Unix only).
   true = is writable.
*/
func checkUXWritable(path string) bool {
	if gOps == OPSUX || gOps == OPSMAC {
		return unix.Access(path, unix.W_OK) == nil
	} else {
		Message("Function only for Linux FS", lumber.ERROR)
	}
	return false
}
