package main

import (
	"net/http"

	"github.com/elazarl/goproxy"

	"github.com/jcelliott/lumber"
)

func orPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func startproxy() {
	proxy := goproxy.NewProxyHttpServer()
	proxy.Verbose = true
	err := http.ListenAndServe(":"+pPort, proxy)
	if err != nil {
		Message(err.Error(), lumber.ERROR)
	}
}
