package main

import (
	"fmt"

	"os"

	"strconv"
	"strings"
	"time"

	"github.com/jcelliott/lumber"
)

/* Error handle. true if OK. false = failed. */
func checkError(e error) bool {
	if e != nil {
		Message(fmt.Sprintf("Error: %v", e), lumber.ERROR)

		//os.Exit(1)
		return false
	}
	return true
}

/* Check if IDs and names are unique.
TODO: make a configuration to disable / enable automated ID change if not unique.
*/
func checkIDs() {
	xmlID++
	mapID := make(map[int]bool)
	mapName := make(map[string]bool)

	for i, c := range cAll {
		if mapName[c.Name] {
			nametocheck := checkName(c.Name)
			Message("Renaming ID "+strconv.Itoa(c.ID)+", name "+c.Name+" to new name "+nametocheck+" to make name unique.", lumber.INFO)
			cAll[i].Name = nametocheck
		} else {
			mapName[c.Name] = true
		}

		if mapID[c.ID] {
			Message("Re-setting ID "+strconv.Itoa(c.ID)+" for "+c.Name+" to new ID: "+strconv.Itoa(xmlID), lumber.WARN)

			//cAll[mIndex[c.Name]].ID = xmlID
			cAll[i].ID = xmlID
			//Message("DEBUG: "+strconv.Itoa(cAll[mIndex[c.Name]].ID), lumber.DEBUG)
			xmlID++
		} else {
			mapID[c.ID] = true
		}
		/* Check if types are in the valid range of mrsl. */
		if _, ok := nrsl[c.Handler]; !ok {
			cAll[i].Handler = HANDLERVALUE
			Message("Corrrected invalid datatype for "+c.Name+" from "+c.Handler+" to "+HANDLERVALUE, lumber.DEBUG)
		}

	}
}

/* Check if a blocker is configured for that data. */
func checkIsBlocked(b Blocker) bool {
	var isBlocked []bool
	var isValid []bool
	var invalid, blocked int
	isBlocked = []bool{false, false, false, false, false}
	isValid = []bool{true, true, true, true, true}
	t := time.Now()
	h := t.Hour()
	w := int(t.Weekday())
	md := t.Day()
	m := int(t.Month())
	/* Check hours */
	if b.Hours != "" {
		isValid[0], isBlocked[0] = checkNumberList(b.Hours, 0, 23, h)
	}
	/* Check week day */
	if b.Weekday != "" {
		isValid[1], isBlocked[1] = checkNumberList(b.Weekday, 0, 6, w)
	}
	/* Check month day */
	if b.Monthday != "" {
		isValid[2], isBlocked[2] = checkNumberList(b.Monthday, 1, 31, md)
	}
	/* Check month */
	if b.Months != "" {
		isValid[3], isBlocked[3] = checkNumberList(b.Months, 1, 12, m)
	}
	/* Check for password */
	if b.Password != "" {
		if pCmdPassword == "" {
			Message("Command is locked by a password.\nPlease enter password with parameter cmdpassword=<yourpassword>", lumber.ERROR)

			isBlocked[4] = true
		} else {
			if decryptTextPwd(b.Password, DEFAULTPASSWORD) == pCmdPassword {
				isBlocked[4] = false
			} else {
				isBlocked[4] = true
			}
		}
	}
	if b.WhitelistUser != "" {
		isBlocked[5] = gCurrentuser != b.WhitelistUser
	}
	for i := 0; i < len(isBlocked); i++ {
		if isBlocked[i] {
			blocked++
		}
		if isValid[i] == false {
			invalid++
		}
	}
	if invalid > 0 {
		Message("Configuration for blocking commands is not valid.\nPlease check that all values are numbers and comma separated.", lumber.ERROR)

		os.Exit(2)
	}
	Message(fmt.Sprintf("Number blocking conditions: %v", blocked), lumber.DEBUG)

	return blocked > 0
}

/* Check if the string contains only numbers with separator command
* and a maximum value of max. max -1 is unlimited.
 */
func checkNumberList(numberlist string, min int, max int, checknr int) (bool, bool) {
	var smin, smax string
	var isValid bool
	var isCheck bool
	smin = strconv.Itoa(min)
	smax = strconv.Itoa(max)
	sha := strings.Split(numberlist, ",")
	isValid = true
	isCheck = false
	for _, s := range sha {
		si, err := strconv.Atoi(strings.Trim(s, " "))
		if err != nil {
			Message("Not a number: "+s, lumber.ERROR)

			isValid = false
		} else if si > max && max > 0 {
			Message("Configured value is not in range ("+smin+"-"+smax+"): "+s, lumber.ERROR)

			isValid = false
		}
		if si == checknr {
			isCheck = true
		}
	}
	return isValid, isCheck
}

/* Check if possibly name exists and if yes find another one adding patter _01. */
//TODO:
/*
 Name exists already, changed name to you_03.
You can change name aftewards with -rename <oldname> <newname>.
 Renaming ID 105, name you_02 to new name you_03 to make name unique.
 Name exists already, changed name to you_03.
You can change name aftewards with -rename <oldname> <newname>.
 Renaming ID 107, name you_02 to new name you_03 to make name unique.
 Data saved to /home/amrit/.gomando/english.xml.

*/
func checkName(args2 string) string {
	if args2 == "" {
		//	if len(flag.Args()) >= 1 {
		//		args2 = flag.Args()[0]
		//	} else {
		args2 = "unknown"
		//	}
	}
	//	else if args2 == "sudo" {
	//		if len(flag.Args()) >= 2 {
	//			args2 = flag.Args()[1]
	//		}
	//	}
	/* Remove spaces and <> from possible name. */
	count1 := len(args2)
	args2 = strings.Replace(args2, " ", "", -1)
	args2 = strings.Replace(args2, ">", "", -1)
	args2 = strings.Replace(args2, "<", "", -1)
	count2 := len(args2)
	if count1 > count2 {
		Message(strconv.Itoa(count1-count2)+" spaces and <> have been removed.", lumber.DEBUG)
	}
	if _, err := strconv.Atoi(string(args2[0])); err == nil && count2 <= 3 {
		Message("Internal name must not start with a number below 1000. Added prefix \"nr\".", lumber.ERROR)
		args2 = "nr" + args2
	}
	//Message("args2: "+string(args2[0]), lumber.DEBUG)

	if nameExists(args2) {
		var isExisting = true
		var sep string
		for isExisting {
			if len(args2) > 3 {
				sep = args2[len(args2)-3 : len(args2)-2]
			} else {
				sep = ""
			}
			if sep == "_" {
				//				p("sep: " + sep)
				var suf = args2[(len(args2) - 2):len(args2)]
				//				p("Suf: " + suf)
				/* if suffix = _00 or similar */
				if sufi, err := strconv.Atoi(suf); err == nil {
					sufi++
					if sufi >= 100 {
						Message("Could not find a unique name for macro.\n Please look manually for a not used name.", lumber.ERROR)

						os.Exit(1)
					}
					suf = strconv.Itoa(sufi)
					if sufi < 10 {
						suf = "0" + suf
					}
					args2 = args2[:len(args2)-2] + suf
					//					fmt.Printf("Sufi: %v", sufi)
					//					fmt.Printf("args2: %v", args2)
				}
			} else {
				args2 = args2 + "_01"
			}
			isExisting = nameExists(args2)
			//TODO change again if exists.
		}
		Message("Name exists already, changed name to "+args2+".", lumber.DEBUG)
		Message("You can change name aftewards with -rename <oldname> <newname>.", lumber.DEBUG)
	}
	return args2
}

/* Checks if a macro name already exists. */
func nameExists(name string) bool {

	_, found1 := dataIndex[name]
	_, found2 := macroIndex[name]
	if found1 {
		Message("Found : "+name, lumber.DEBUG)
	} else {
		Message("Not found: "+name, lumber.DEBUG)
	}
	return found1 || found2
}
